<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddpaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addpayments', function (Blueprint $table) {
            $table->id();
            $table->string('addpayment_unique_id')->unique();
            $table->string('addpayment_payments_unique_id');
            $table->string('addpayment_user_unique_id');
            $table->json('part_payment_detatil');
            $table->String('payment_status');
            $table->String('payment_start_date');
            $table->String('payment_end_date');
            $table->String('payment_amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addpayment');
    }
}
