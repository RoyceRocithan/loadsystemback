<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('users_unique_id')->unique();
            $table->string('name');
            $table->string('is_role')->nullable();
            $table->string('email')->unique();
            $table->string('Shop_name')->nullable(true);
            $table->string('Phonenumber');
            $table->string('Ic_number');
            $table->String('Profile_picture')->nullable(true);
            $table->string('delete_permition')->nullable(true);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
