<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->string('payments_unique_id')->unique();
            $table->String('payments_category_id');
            $table->json('payment_detatil');
            $table->String('payment_status');
            $table->json('withdraws')->nullable(true);
            $table->String('Divide')->nullable(true);
            $table->String('payment_start_date');
            $table->String('payment_end_date');
            $table->String('create_by');
            $table->boolean('displayed')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment');
    }
}
