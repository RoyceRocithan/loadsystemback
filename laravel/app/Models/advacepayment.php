<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class advacepayment extends Model
{
    use HasFactory;
    protected $fillable = [
        'advacepayments_unique_id',
        'advacepayment_users_unique_id',
        'amount',
        'date'
         ];
}
