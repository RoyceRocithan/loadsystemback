<?php

namespace App\Models;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;
    protected $fillable = [
        'payments_unique_id',
        'payments_category_id',
        'create_by',
        'payment_status',
        'payment_start_date',
        'payment_end_date',
        'displayed',
        'payment_detatil' ,
        'user_unique_id',
        'withdraws',
        'Divide'
         ];
        //  public function username() {
        //     return $this->hasone(User::class, 'create_by', 'unique_id'); 
           
          
        // }
}
