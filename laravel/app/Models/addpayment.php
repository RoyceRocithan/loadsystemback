<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class addpayment extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'addpayment_unique_id',
        'addpayment_payments_unique_id',
        'part_payment_detatil',
        'payment_status',
        'payment_start_date',
        'payment_end_date',
        'payment_amount'
         ];
}
