<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Orderpay extends Model
{
    use HasFactory;
    protected $fillable = [
        'orderpays_categories_unique_id',
        'oder',
        'created_at',
        'updated_at'
         ];
}
