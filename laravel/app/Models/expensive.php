<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class expensive extends Model
{
    use HasFactory;
    protected $fillable = [
        'expensives_users_unique_id',
        'expensives_date',
        'expensives_amount',
        'Discription'
         ];
}
