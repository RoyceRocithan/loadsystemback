<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Payment;
use Redirect;
class CategoryController extends Controller
{
    //
    public function caterories(Request $request){
        $data =Category::leftJoin('users', 'categories.create_by', '=', 'users.users_unique_id')->get();
       return view('pages.caterories', ['data'=>$data]);
    }
    public function cateories_sub(Request $request){
     
      try {

        $validator = Validator::make($request->all(), [
            'amount' => 'required|unique:categories|numeric|min:0',
            ]);

        if($validator->fails()){
            return Redirect::back()->withErrors($validator);
        }
       
        $request['categories_unique_id'] = uniqid();
        $request['create_by'] = Auth::user()->users_unique_id;
      
        $user = Category::create($request->toArray());
        

        return Redirect::back()->with('Successfull message', 'Amount Added!');


     }catch(Exception $error){
        return Redirect::back()->withErrors($error);
    }
    }
    public function getUserDetail($id){
      return '';
    }
    public function Userview(Request $request){
       $id = $request->get('unque');
       $sql = Payment::where('payments_unique_id',$id)->get()->toarray();
       $tu = $sql[0]['payment_detatil'];
       $u = User::select('name')->whereIn('users_unique_id',json_decode($tu))->get()->toArray();
       $returnHTML = view('pages.replayuserview')->with(['unique' =>$u])->render();
       return response()->json(['data'=>$returnHTML]);
    }
}
