<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\Models\User;
use App\Models\Category;
use App\Models\Payment;
use App\Models\addpayment;
use Illuminate\Support\Str;
use Redirect;
use Session;
use DB;

class AddpaymentController extends Controller
{
    private $yoo = [];
    private $lastkey=[];
    private $amountofpackdetail=[];
    private $packageamount =[];
    private $packagblanceamount=[];
    private $packagestatus =[];
    private $activeround =[];
    private $countofuser =[];
    private $packageamountpri = [] ;
    private $countacc=[];
    //
    public function Add_payment_form(){
        return View::make('pages.Add_payment');
    }
   
    public function findname(Request $request){
        $name = $request->get('name');
        $uo = User::select('users_unique_id')->where('name', 'LIKE', "$name%")->get()->toArray();
       $urs = $uo[0]['users_unique_id'];
        $user = DB::table('addpayments')->where('addpayments.payment_status','active')
          //  ->whereIn('addpayments.addpayment_user_unique_id',[$urs] )
            ->whereIn('addpayment_user_unique_id',$uo )
             ->leftJoin('payments', 'payments.payments_unique_id', '=', 'addpayments.addpayment_payments_unique_id')
             ->leftJoin('categories','categories.categories_unique_id','=','payments.payments_category_id')
             ->leftJoin('users','users.users_unique_id','=','addpayments.addpayment_user_unique_id')
             ->get();
             
         return view('pages.replay', ['users' => $user]);
    }
    public function UpdateAmount(Request $request){
      
       $id = $request->get('id');
       $addamount = $request->get('amount');
       $querycheck  =  DB::table('addpayments')->select('payment_status')->where('addpayment_unique_id','=',$id)->first();
      if($querycheck->payment_status=='inactive'){
         return '<label class="text-danger text-lg-center">Account Closed </lable>';
      }
       $query  =  DB::table('addpayments')->select('part_payment_detatil')->where('addpayment_unique_id','=',$id)->first();
       if ($query === null || empty($addamount)) {
        return '<label class="text-danger text-lg-center">Enter the Amount </lable>';
       }else{
        $query1  =  DB::table('addpayments')->select('payment_amount')->where('addpayment_unique_id','=',$id)->first();
         if($query1->payment_amount<$addamount){
            return '<label class="text-danger text-lg-center">Large Amount </lable>';
        }else{
            $amount = [];
            foreach ($query as  $key => $blog) {
              foreach ((object)$blog as  $value) {
              $i= json_decode( $value);
              foreach ($i as $key => $value2) {
                  array_push($amount,$value2->paymentdetail->amount);
                  }
              }
             $totalamount=array_sum($amount); 
            }
            if((float)$totalamount<=0){
                $date = date('Y-m-d H:i:s');
                  $paymentd = array(array('paymentdetail' => array('pay_id'=>$id,'paymentdetail_unique_id' => uniqid() ,'amount' => (float)$addamount,'paydate' => $date,'create_by'=>Auth::user()->users_unique_id),) );
                  addpayment::where('addpayment_unique_id', $id)
                              ->update([
                                  'part_payment_detatil' => $paymentd
                                 ]);
                 (float)$query1->payment_amount===(float)$addamount?
                 addpayment::where('addpayment_unique_id', $id)
                              ->update([
                                  'payment_status' => 'inactive'
                                 ]):'';

                return '<label class=" text-green text-lg-center">Updated Amount </lable>';
                            
              

            }else{
                 if((float)$query1->payment_amount<(float)$totalamount+$addamount){
                return '<label class="text-danger text-lg-center">Large Amount </lable>';
              }else{
                $date = date('Y-m-d H:i:s'); 
                $paymentdarray = array(array('paymentdetail' => array('pay_id'=>$id,'paymentdetail_unique_id' => uniqid() ,
                    'amount' => (float)$addamount, 'paydate' => $date,'create_by'=>Auth::user()->users_unique_id ), ));
                 $amountarray = [];
                 $namequery = DB::table('addpayments')->select('part_payment_detatil')->where('addpayment_unique_id','=',$id)->get();
                 foreach ($namequery as  $key => $blogname) {
                   foreach ((object)$blogname as  $valuename) {
                    $i= json_decode( $valuename);
                    foreach ($i as $key => $valuearray) {
                     array_push($amountarray,$valuearray) ;
                      }
                   }
                    }
                 $st=[];
                 array_push($amountarray,json_decode(json_encode($paymentdarray[0])));
                  addpayment::where('addpayment_unique_id', $id)
                                        ->update([
                                            'part_payment_detatil' => json_encode($amountarray)
                                          ]);
                                          (float)$query1->payment_amount===(float)$totalamount+$addamount?
                 addpayment::where('addpayment_unique_id', $id)
                              ->update([
                                  'payment_status' => 'inactive'
                                 ]) :'';
                 return '<label class=" text-green text-lg-center">Updated Amount </lable>';                         
              }
            }
        }
       }
     
    }
     public function ReturnAmount(Request $request){
      $id = $request->get('id');
      $addamount = $request->get('amount');
      $querycheck  =  DB::table('addpayments')->select('payment_status')->where('addpayment_unique_id','=',$id)->first();
      if($querycheck->payment_status=='inactive'){
         return '<label class="text-danger text-lg-center">Account Closed </lable>';
      }
      $query  =  DB::table('addpayments')->select('part_payment_detatil')->where('addpayment_unique_id','=',$id)->first();
      if ($query === null || empty($addamount)) {
       return '<label class="text-danger text-lg-center">Enter the Amount </lable>';
      }else{
       $query1  =  DB::table('addpayments')->select('payment_amount')->where('addpayment_unique_id','=',$id)->first();
        if($query1->payment_amount<$addamount){
           return '<label class="text-danger text-lg-center">Large Amount </lable>';
       }else{
           $amount = [];
           foreach ($query as  $key => $blog) {
             foreach ((object)$blog as  $value) {
             $i= json_decode( $value);
             foreach ($i as $key => $value2) {
                 array_push($amount,$value2->paymentdetail->amount);
                 }
             }
            $totalamount=array_sum($amount); //return ammount
           }
           if((float)$totalamount<=0){
               $date = date('Y-m-d H:i:s');
                 $paymentd = array(array('paymentdetail' => array('pay_id'=>$id,'paymentdetail_unique_id' => uniqid() ,'amount' => (float)$addamount,'paydate' => $date,'create_by'=>Auth::user()->users_unique_id),) );
                 return '<label class="text-danger text-lg-center">Amout Fell Empty </lable>';
             }else{
                if((float)$query1->payment_amount<(float)$totalamount+$addamount){
               return '<label class="text-danger text-lg-center">Large Amount </lable>';
             }else{
               $date = date('Y-m-d H:i:s'); 
               $paymentdarray = array(array('paymentdetail' => array('pay_id'=>$id,'paymentdetail_unique_id' => uniqid() ,
                   'amount' => (float)$addamount, 'paydate' => $date,'create_by'=>Auth::user()->users_unique_id ), ));
                $amountarray = [];
                $namequery = DB::table('addpayments')->select('part_payment_detatil')->where('addpayment_unique_id','=',$id)->get();
                foreach ($namequery as  $key => $blogname) {
                  foreach ((object)$blogname as  $valuename) {
                   $i= json_decode( $valuename);
                   foreach ($i as $key => $valuearray) {
                    array_push($amountarray,$valuearray) ;
                     }
                  }
                   }
                $st=[];
                array_push($amountarray,json_decode(json_encode($paymentdarray[0])));
                //  addpayment::where('addpayment_unique_id', $id)
                //                        ->update([
                //                            'part_payment_detatil' => json_encode($amountarray)
                //                          ]);
                //                          (float)$query1->payment_amount===(float)$totalamount+$addamount?
                // addpayment::where('addpayment_unique_id', $id)
                //              ->update([
                //                  'payment_status' => 'inactive'
                //                 ]) :'';
                return '<label class=" text-green text-lg-center">Updated Amount </lable>';                         
             }
           }
       }
      }
     }
     public function Roundadd(Request $request){
        $op =array();
        $id = $request->get('id');
         $name = DB::table('payments')->select('withdraws')->where('payments_unique_id','=',$id)->get()->toArray();
        $ys = json_decode(json_encode($name), true);
         foreach ($ys as $key => $valuetow) {
         foreach ($valuetow as $key => $to) {
          $yu = json_decode($to);
           foreach ($yu as $key => $s) {
            $this->roundarray($s);
           
           }
           array_push($op,$yu);
          $returnHTML = view('pages.replayround')->with(['userjobs'=> $op,'unique' =>$id])->render();
          return response()->json(['data'=>$returnHTML]);
         }
        }
     }
     public function roundarray($t){
      $result = array();
      // $ut = array_column($t,'one');
      // foreach ($ut as $key => $value) {
      //   print_r($value);
      //   }
       
     }
     public function CashAmount(Request $request){
       $uty = [];
       $id = $request->get('id');
       $round = $request->get('round');
       $unque = $request->get('unque');
        $name = addpayment::select('part_payment_detatil')->where('addpayment_unique_id',$unque)->get()->toArray();
       $ys = json_decode(json_encode($name), true);
       foreach ($ys as $key => $value1) {
        foreach ($value1 as $key => $values) {
           $y = json_decode($values);
          foreach ($y as $key => $valuesr) {
             $this->addcashAmount($valuesr,$id,$round,$unque);
            array_push($uty,$valuesr);

          }
        }
      }
      addpayment::where('addpayment_unique_id', $unque)
      ->update([
          'part_payment_detatil' => json_encode($uty)
        ]);
     }
     public function addcashAmount($valuesr,$id,$round,$unque){
      $ut = array_column($valuesr,$this->getIndianCurrency($round));
       foreach ($ut as $key => $value1) {
           $trs = $value1->paymentdetail;
           foreach ($trs as $key => $value) {
                 if($value->paymentdetail_unique_id==$id){
                   $value->moneystatus='credit';
                 }
               
           } 
       }
      //print_r($ut);
     return $ut;
    
     }


     public function withdorw(Request $request){
       
      $username  = $request->get('username');
      $amount    = $request->get('amount');
      $packageid = $request->get('packageid');
      $round     = $request->get('round');
      $unique    = $request->get('unique');
      $userid    = $request->get('userid');
      $package_payid = $request->get('package_payid');
      $packageamount = $request->get('packageamount');
      $packagestatus = $request->get('packagestatus');
      $userselectkey = $request->get('userselectkey');
      $total_me      = $request->get('total_me');//member of partsipate
      $cat_amount    = $request->get('cat_amount');//catorgry amount
      $Divide        = $request->get('Divide');//Divide
      
     
      $lasrarray = [];
      if ( ($userid ==NULL)||($amount ==NULL) ) {
        return response()->json(['Error' => 'Empty error'], 200);
      }else{
          if((int)$packageamount<$amount){
            return response()->json(['Error' => 'Large Amount'], 200);
          }else{
                if($packagestatus==='Complete'){
                  return response()->json(['Error' => 'This package Completed'], 200);
                }else{
                     $usergfh = DB::table('payments')->select('payment_detatil','withdraws')->where('payments_unique_id','=',$unique)->get()->toArray(); 
                     $ion = $request->get('userselectkey')==NULL?(array)$userid:array_merge((array)$userid,$userselectkey);
                      $hgj = array_filter($ion);
                      $tyuj = array_count_values($hgj);
                       $tohf = json_decode($usergfh[0]->payment_detatil);
                       array_push($this->countofuser,count($tohf));
                      $tyuty = array_count_values($tohf);
                       array_push($this->packageamountpri,$packageamount);
                     if($tyuty[$userid]<$tyuj[$userid]){
                        return response()->json(['Error' => 'This user package Completed'], 200);
                      }
                    if($total_me==$round){
                      $tus = [];
                      $test = DB::table('addpayments')->select('part_payment_detatil')->where('addpayment_payments_unique_id','=',$unique)->get()->toArray();
                      $t = json_decode(json_encode($test,true));
                      foreach ($t as $key => $value) {
                        foreach ($value as $key => $value1) {
                                $g = json_decode($value1,true);
                               foreach ($g as $key => $value2) {
                                 foreach ($value2 as $key => $value3) {
                                    foreach ($value3 as $key => $value4) {
                                      $value4['packageDiscription']['packagestatus']=='Complete'?array_push($tus,'Complete'):array_push($tus,'inComplete');
                                    }
                                 }
                              }
                                                     
                        }
                      
                      }
                      if (in_array('inComplete', $tus, true)) { //  print_r('khj') ;
                        $name = DB::table('payments')->select('withdraws')->where('payments_unique_id','=',$unique)->get()->toArray();
                        $t = json_decode(json_encode($name,true));
                        foreach ($t as $key => $value1) {
                          foreach ($value1 as $key => $values) {
                            $y = json_decode($values);
                            foreach ($y as $key => $valuesr) {
                                 $this->count_test($valuesr,$round);
                            }
                          }
                        }
                        // $dfrty = json_decode($usergfh[0]->withdraws);
                        // $fgh = json_decode(json_encode($test,true));
                        // print('<pre>');
                        // print_r($fgh);
                        // print('</pre>');
                        //return null;
                        //withdrow count 
                        //one withdrow 
                        //if condition
                          $co = (int)$this->countacc[0];
                          if($co==NULL){
                            $name = DB::table('payments')->select('withdraws')->where('payments_unique_id','=',$unique)->get()->toArray();
                            $t = json_decode(json_encode($name,true));
                            foreach ($t as $key => $value1) {
                              foreach ($value1 as $key => $values) {
                                $y = json_decode($values);
                                foreach ($y as $key => $valuesr) {
                                     $this->count_update($valuesr,$round,$amount,$userid,$username);
                                    
                                }
                              } //array_push($y,$valuesr);
                            }
                            payment::where('payments_unique_id', $unique)
                            ->update([
                              'withdraws' => json_encode($y)
                              ]); 
                              return response()->json(['sucess' =>$this->activeround], 200);
                              die();
                            // print('<pre>');
                            // print_r($y);
                            // print('</pre>');
                               //return response()->json(['Error' => 'This package Have credit insert ..'], 200);  
                               //$this->nextround($lasrarray,($round+1),$package_payid,$unique,$amount,$cat_amount,$Divide,$total_me);
                                //return response()->json(['Error' => 'This package Have credit insert ..'], 200);
                                //die();  
                           }else{
                             return response()->json(['Error' => 'This package Have credit already ..'], 200);
                           }
                          
                        
                      }else{
                        
                        payment::where('payments_unique_id', $unique)
                        ->update([
                            'payment_status' => 'Complete'
                          ]);
                          addpayment::where('addpayment_payments_unique_id', $unique)
                          ->update([
                              'payment_status' => 'Complete'
                            ]);  

                      }
                      $name = DB::table('payments')->select('withdraws')->where('payments_unique_id','=',$unique)->get()->toArray();
                      $t = json_decode(json_encode($name,true));
                      foreach ($t as $key => $value1) {
                        foreach ($value1 as $key => $values) {
                          $y = json_decode($values);
                          foreach ($y as $key => $valuesr) {
                            $this->withdorwfilter($valuesr,$round,$package_payid,$amount,$userid,$total_me,$username);
                            array_push($lasrarray,$valuesr);
                          }
                        }
                            
                      }
                      $this->nextround($lasrarray,($round+1),$package_payid,$unique,$amount,$cat_amount,$Divide,$total_me);
                     return response()->json(['sucess' =>$this->activeround], 200);
                    } else{
                      $name = DB::table('payments')->select('withdraws')->where('payments_unique_id','=',$unique)->get()->toArray();
                      $t = json_decode(json_encode($name,true));
                      foreach ($t as $key => $value1) {
                        foreach ($value1 as $key => $values) {
                          $y = json_decode($values);
                          foreach ($y as $key => $valuesr) {
                            $this->withdorwfilter($valuesr,$round,$package_payid,$amount,$userid,$total_me,$username);
                            array_push($lasrarray,$valuesr);
                          }
                        }
                            
                      }
                      $this->nextround($lasrarray,($round+1),$package_payid,$unique,$amount,$cat_amount,$Divide,$total_me);
                     return response()->json(['sucess' =>$this->activeround], 200);
                    }
                     //
                     
                      
                  }
              
           

            }
            
              }
        
      }
      public function count_update($valuesr,$round,$amount,$userid,$username){
        $ut = array_column($valuesr,$this->getIndianCurrency($round));
        foreach ($ut as $key => $value1) {
          foreach ($value1->paymentdetail as $key => $value2) {
            $value2->amount = $amount;
            $value2->status=1;
            $value2->get_by=$userid;
            $value2->get_date = date('yy-m-d');
            $value2->create_by =Auth::user()->users_unique_id;
            $value2->blance=(int)$value1->packageDiscription->packageamount - (int)$amount;
            $value2->username =$username;
           }
           array_push($this->activeround,$value1->paymentdetail);
        }
       return $ut;
      }
      public function count_test($valuesr,$round){
        $ut = array_column($valuesr,$this->getIndianCurrency($round));
        foreach ($ut as $key => $value1) {
          foreach ($value1->paymentdetail as $key => $value2) {
               array_push($this->countacc,$value2->amount);
          }

        }

      }
     
      public function withdorwfilter($s,$round,$package_payid,$amount,$userid,$total_me,$username){
      $ut = array_column($s,$this->getIndianCurrency($round));
      foreach ($ut as $key => $value1) {
        $value1->packageDiscription->packagestatus ='Complete';
        foreach ($value1->paymentdetail as $key => $value2) {
           if($value2->pay_id==$package_payid){
            $value2->amount=$amount;
            $value2->status=1;
            $value2->get_by=$userid;
            $value2->get_date = date('yy-m-d');
            $value2->create_by =Auth::user()->users_unique_id;
            $value2->blance=(int)$value1->packageDiscription->packageamount - (int)$amount;
             array_push($this->packagblanceamount,(int)$value1->packageDiscription->packageamount - (int)$amount);
          }else false;
          
            foreach ($value1->paymentdetail as $key => $value) {
             $value->username = $username;
            }
           array_push($this->activeround,$value1->paymentdetail);
        }
      }
      return $ut;
     }
     public function nextround($arr,$round,$package_payid,$unique,$amount,$cat_amount,$Divide,$total_me){
       $yo = [];
       foreach ($arr as $key => $value) {
         $this->nextsubround($value,$round,$package_payid,$unique,$amount,$cat_amount,$Divide,$total_me);
         array_push($yo,$value);
       }
        //print_r($yo);
       //query write.......................................
       payment::where('payments_unique_id', $unique)
                                        ->update([
                                            'withdraws' => json_encode($yo)
                                          ]);
      $this->addpaynode(($round),$unique,$package_payid,$amount,$cat_amount,$Divide,$total_me);
     }
     public function addpaynode($round,$unique,$package_payid,$amount,$cat_amount,$Divide,$total_me){
       $yop = [];
      $name = DB::table('addpayments')->select('addpayment_unique_id','part_payment_detatil','addpayment_payments_unique_id')->where('addpayment_payments_unique_id','=',$unique)->get()->toArray();
       foreach ($name as $key => $value) {
          $name = DB::table('addpayments')->select('part_payment_detatil')->where('addpayment_unique_id','=',$value->addpayment_unique_id)->get()->toArray();

        $ys = json_decode(json_encode($name), true);
          foreach ($ys as $key => $value1) {
           foreach ($value1 as $key => $values) {
              $y = json_decode($values);
            foreach ($y as $key => $valuesr) {
                     $this->packageadd($valuesr,$round,$package_payid,$value->addpayment_unique_id,$amount,$cat_amount,$Divide,$total_me);
                     array_push($yop,$valuesr); 
                    }
                    addpayment::where('addpayment_unique_id', $value->addpayment_unique_id)
                    ->update([
                        'part_payment_detatil' => json_encode($yop)
                      ]);
                      $yop=[];
           } 
          
           //print_r($yop);
        }
       
           
       }
      
     
     
    }
    public function packageadd($s,$round,$package_payid,$tt,$amount,$cat_amount,$Divide,$total_me){
     $ut = array_column($s,$this->getIndianCurrency($round));
      foreach ($ut as $key => $value1) {
      if($value1->packageDiscription->package_payid==$tt){
          $value1->packageDiscription->packageamount=$Divide==='on'?(int)($amount)/(int)($this->countofuser[0]):(($amount)/$total_me);
          $value1->packageDiscription->countpackageamount=$Divide==='on'?(int)($amount)/(int)($this->countofuser[0]):(($amount)/$total_me);
          $value1->packageDiscription->status=1;
          $value1->packageDiscription->packagestatus='processing';
          }else false;
        
       
      } return $ut;
      
     
    
    }
     public function nextsubround($s,$round,$package_payid,$unique,$amount,$cat_amount,$Divide,$total_me){
      
        $ut = array_column($s,$this->getIndianCurrency($round));
        foreach ($ut as $key => $value1) {
         if($value1->packageDiscription->package_payid==$package_payid){
          $value1->packageDiscription->packageamount=(int)($this->packageamountpri[0]);
          $value1->packageDiscription->status=1;
         }else false;
         
         
        }
        return $ut;
       
       
     }
     public function partAmount(Request $request){
       $addpayment_payments_unique_id = $request->get('addpayment_payments_unique_id');
       $catamount = (float)$request->get('catamount');
       $divide    = $request->get('divide');
       $amount = (float)$request->get('amount');
       $id = $request->get('id');
       $partamountdate = $request->get('partamountdate');
       if($partamountdate==NULL || $amount==NULL){
        return response()->json(['Error' => 'Empty error'], 200);
       }
       $round = (float)$request->get('round');
       $tof= [];
       $name = DB::table('addpayments')->select('part_payment_detatil')->where('addpayment_unique_id','=',$id)->get()->toArray();
       $ys = json_decode(json_encode($name), true);
         foreach ($ys as $key => $value1) {
          foreach ($value1 as $key => $values) {
             $y = json_decode($values);
           foreach ($y as $key => $valuesr) {
               $this->test($valuesr,$id,$amount,$partamountdate,$round,$addpayment_payments_unique_id);
                array_push($tof,$valuesr);
                
           }
          }
       }//(int)$this->packageamount[0]
       if((int)$this->packageamount[0]< (array_sum($this->amountofpackdetail)+$amount)){
         return response()->json(['Error' => 'Large amount'], 200);
       }else{
             if($this->packagestatus[0]==='Complete'){
              return response()->json(['Error' => 'this package is Completed'], 200);
             }else{

                  // payment->notcomplete
                  //
           addpayment::where('addpayment_unique_id', $id)
            ->update([
                  'part_payment_detatil' => json_encode($tof)
                   ]) ;
         $last =end($this->yoo);
         $pre =prev($this->yoo);
      
       $final = array_merge(end($this->yoo),$this->lastkey[0]);
           $rtvb=[];
            $query2 = addpayment::select('part_payment_detatil')->where('addpayment_payments_unique_id',$addpayment_payments_unique_id)->get()->toArray();
             foreach ($query2 as $key => $value) {
               $t = $value['part_payment_detatil'];
               $ys = json_decode($t);
               foreach ($ys as $key => $value) {
                foreach ($value as $key => $value) {
                  $ys = json_decode(json_encode($value), true);
                  foreach ($ys as $key => $value1) {
                    foreach ($value1 as $key => $value) {
                    
                    ($value1['packageDiscription']['packagestatus']==='processing'||$value1['packageDiscription']['packagestatus']==='')?array_push($rtvb,'processing'):array_push($rtvb,'complete');
                     
                    }
                  }
                }
               }
               
             }
            
            
           
             if (in_array("processing", $rtvb))
                  {
                    return response()->json(['Sucess' => $final],200);
                  }
                else
                  {
                    $name = DB::table('payments')->select('withdraws')->where('payments_unique_id','=',$addpayment_payments_unique_id)->get()->toArray();
                    $t = json_decode(json_encode($name,true));
                    foreach ($t as $key => $value1) {
                      foreach ($value1 as $key => $values) {
                        $y = json_decode($values);
                        foreach ($y as $key => $valuesr) {
                             $this->namei($valuesr,$round);
                        }
                      }
                    }
                    payment::where('payments_unique_id',$addpayment_payments_unique_id)
                    ->update([
                        'withdraws' => json_encode($y)
                      ]) ;
                      payment::where('payments_unique_id',$addpayment_payments_unique_id)
                    ->update([
                        'payment_status' => 'Complete'
                      ]) ;
                      addpayment::where('addpayment_payments_unique_id',$addpayment_payments_unique_id)
                      ->update([
                          'payment_status' => 'Complete'
                        ]) ;

                    return response()->json(['Sucess' => $final],200);


                  }
             //write code 
              //1.complete ceck
              //2.round check 
              //3.complete withrow json
              //4.update payment status
               // $this->payststus($uo,$id,$amount,$partamountdate,$round,$addpayment_payments_unique_id);
         //return response()->json(['Sucess' => $final],200);

             }
        // return $this->packagestatus[0];
      //      addpayment::where('addpayment_unique_id', $id)
      // ->update([
      //     'part_payment_detatil' => json_encode($tof)
      //    ]) ;
      //    $last =end($this->yoo);
      //    $pre =prev($this->yoo);
      
      //  $final = array_merge(end($this->yoo),$this->lastkey[0]);
      //  return response()->json(['Sucess' => $final],200);
         //return response()->json(['sucess'=>'jhg'], 200,);
       }
      //  addpayment::where('addpayment_unique_id', $id)
      // ->update([
      //     'part_payment_detatil' => json_encode($tof)
      //    ]) ;
      //    $last =end($this->yoo);
      //    $pre =prev($this->yoo);
      
       //$final = array_merge(end($this->yoo),$this->lastkey[0]);
       //return response()->json(['Sucess' => $final],200);
        //print_r(array_sum($this->amountofpackdetail));
         //print_r($op);
        //print_r($this->packageamount);

     
     }
     public function namei($value1,$round){
      $ut = array_column($value1,$this->getIndianCurrency($round)); 
      foreach ($ut as $key => $value1) {
        $value1->packageDiscription->packagestatus ='Complete';
       
      }
      return $ut;
      
     }
     public function DeleteAmount(Request $request){
        $tof= [];
        $name = DB::table('addpayments')->select('part_payment_detatil')->where('addpayment_unique_id','=', $request->get('unque'))->get()->toArray();
        $ys = json_decode(json_encode($name), true);
          foreach ($ys as $key => $value1) {
           foreach ($value1 as $key => $values) {
              $y = json_decode($values);
            foreach ($y as $key => $valuesr) {
                 $this->slll($valuesr,$request->get('id'),$request->get('round'));
                 array_push($tof,$valuesr); 
            }
           }
        }
        addpayment::where('addpayment_unique_id', $request->get('unque'))
        ->update([
            'part_payment_detatil' => json_encode($tof)
           ]) ;
      return response()->json($tof, 200,);
    }
    
  public function slll($t,$id,$round){

$ut = array_column($t,$this->getIndianCurrency($round));
    $uov=[]  ;
    $yof=[];
    $tu=array_push($uov,$ut);
    foreach ($uov as $key => $valueer) {
           foreach ($valueer as $key => $value) {
                  foreach ($value->paymentdetail as $key => $valuea) {
                    if ($valuea->paymentdetail_unique_id ==$id) {
                     unset($value->paymentdetail[$key]); 
                    }
                   Sort($value->paymentdetail); 
               
              }   
             array_push($yof,$value->paymentdetail) ;
           }
        }
return array_values($yof);
}
     
     
   public function test($uo,$id,$amount,$partamountdate,$round,$addpayment_payments_unique_id){
    $cal =[];
    $array =array(
             'pay_id' =>$id,     
             'paymentdetail_unique_id' => uniqid(),    
             'amount' =>$amount,
             'paydate' => $partamountdate,
             'status' => 1,
             'moneystatus'=>'',
             'get' => '',
             'create_by'=>Auth::user()->users_unique_id
    );
    array_push($this->yoo,$array);
    $ut = array_column($uo,$this->getIndianCurrency($round));
    foreach ($ut as $key => $value) {
      $uoh=(array)$value->packageDiscription;
       
           $tp =array('package_uniqueid'=>$uoh['package_uniqueid']);
           
            foreach ((array)$value->paymentdetail as $key => $valueu) {
                 $uio =(array)$valueu;
                  array_push($this->amountofpackdetail,(int)$uio['amount']);
                  array_push($cal,$valueu->amount);
                 // print_r((int)$uio['amount']).'</br>';
            }
            if($value->packageDiscription->packageamount==(array_sum($cal)+$amount)){
              $value->packageDiscription->packagestatus='Complete';
            
            }else{
              false;
            }
            array_push($this->packageamount,$uoh['packageamount']);
            array_push($this->packagestatus,$uoh['packagestatus']);
            array_push($this->lastkey,$tp);
            array_push($value->paymentdetail,$array);
           
           
     }
    
     return $uo;
    }
  public function payststus($uo,$id,$amount,$partamountdate,$round,$addpayment_payments_unique_id){
   $name = payment::select('withdraws')->where('payments_unique_id',$addpayment_payments_unique_id)->get()->toArray();
   $t = json_decode(json_encode($name,true));
                      foreach ($t as $key => $value1) {
                        foreach ($value1 as $key => $values) {
                          $y = json_decode($values);
                          foreach ($y as $key => $valuesr) {
                           print('<pre>');
                           print_r($valuesr);
                           print('</pre>');
                           //$this->filterround($valuesr,$round);
                          }
                        }
                            
                      }
                      die();
   
  }
  // public function filterround($valuesr,$round){
  //  print('<pre>');
  //  print_r($valuesr);
  //  print('<pre>');
  //  die();
  // }

   public function getIndianCurrency(float $number)
{
    $decimal = round($number - ($no = floor($number)), 2) * 100;
    $hundred = null;
    $digits_length = strlen($no);
    $i = 0;
    $str = array();
    $words = array(0 => '', 1 => 'one', 2 => 'two',
        3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
        7 => 'seven', 8 => 'eight', 9 => 'nine',
        10 => 'ten', 11 => 'eleven', 12 => 'twelve',
        13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
        16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
        19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
        40 => 'forty', 50 => 'fifty', 60 => 'sixty',
        70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
    $digits = array('', 'hundred','thousand','lakh', 'crore');

    while( $i < $digits_length ) {
        $divider = ($i == 2) ? 10 : 100;
        $number = floor($no % $divider);
        $no = floor($no / $divider);
        $i += $divider == 10 ? 1 : 2;
        if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
        } else $str[] = null;
    }

    $round = implode('',array_reverse($str));
    $paise = '';

    if ($decimal) {
        $paise = 'and ';
        $decimal_length = strlen($decimal);

        if ($decimal_length == 2) {
            if ($decimal >= 20) {
                $dc = $decimal % 10;
                $td = $decimal - $dc;
                $ps = ($dc == 0) ? '' : '-' . $words[$dc];

                $paise .= $words[$td] . $ps;
            } else {
                $paise .= $words[$decimal];
            }
        } else {
            $paise .= $words[$decimal % 10];
        }

        $paise .= ' paise';
    }

    return (trim($round) ? trim($round) :'') . trim($paise) ;
  }
 
}

