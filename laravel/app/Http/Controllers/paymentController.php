<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\Models\User;
use App\Models\Category;
use App\Models\Payment;
use App\Models\addpayment;
use Illuminate\Support\Str;
use Redirect;
use Session;
use DB;

class paymentController extends Controller
{
    public function Add_cat_money_form(){
        $payment = Payment::select('*')->where('payment_status','active')
        ->leftJoin('users', 'payments.create_by', '=', 'users.users_unique_id')
        ->leftJoin('categories', 'payments.payments_category_id', '=', 'categories.categories_unique_id')->get();
        $user = User::select('users_unique_id','name')->get();
        $cate = Category::select('categories_unique_id','amount')->get();
        return View::make('pages.Add_cat_money',['users' => $user,'cat'=>$cate,'payment'=>$payment]);
    }
    public function Add_cat_money_sub(Request $request){
        

        function getIndianCurrency(float $number)
        {
            $decimal = round($number - ($no = floor($number)), 2) * 100;
            $hundred = null;
            $digits_length = strlen($no);
            $i = 0;
            $str = array();
            $words = array(0 => 'zero', 1 => 'one', 2 => 'two',
                3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
                7 => 'seven', 8 => 'eight', 9 => 'nine',
                10 => 'ten', 11 => 'eleven', 12 => 'twelve',
                13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
                16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
                19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
                40 => 'forty', 50 => 'fifty', 60 => 'sixty',
                70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
            $digits = array('', 'hundred','thousand','lakh', 'crore');
        
            while( $i < $digits_length ) {
                $divider = ($i == 2) ? 10 : 100;
                $number = floor($no % $divider);
                $no = floor($no / $divider);
                $i += $divider == 10 ? 1 : 2;
                if ($number) {
                    $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                    $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                    $str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
                } else $str[] = null;
            }
        
            $round = implode('',array_reverse($str));
            $paise = '';
        
            if ($decimal) {
                $paise = 'and ';
                $decimal_length = strlen($decimal);
        
                if ($decimal_length == 2) {
                    if ($decimal >= 20) {
                        $dc = $decimal % 10;
                        $td = $decimal - $dc;
                        $ps = ($dc == 0) ? '' : '-' . $words[$dc];
        
                        $paise .= $words[$td] . $ps;
                    } else {
                        $paise .= $words[$decimal];
                    }
                } else {
                    $paise .= $words[$decimal % 10];
                }
        
                $paise .= ' paise';
            }
        
            return (trim($round) ? trim($round) :'zero') . trim($paise) ;
        }
        
        
                $array1 = $request->get('payment_detatil');
                $array2 = $request->get('duplicate');
                $dived = $request->get('Divide');
                $result = $array2 == NULL?$array1:array_merge($array1, $array2);
                // return $result;
                // die();
                $ypp =  $result;
                $paymentid = $request->get('category_id');
                $yuu = Category::where('categories_unique_id','=',$paymentid)->get();
                // return $yuu[0]['amount']/count($ypp);
                //  die();
                  if($request->get('payment_start_date')!==NULL){
                    $tjh = $request->get('payment_start_date');
                    $headers = explode('-', $tjh);
                    $rr = $headers[0];
                    $yiuy= (explode('/', $rr));
                    $startdate = $yiuy[1].'-'.$yiuy[0].'-'.$yiuy[2];
                    $rt = $headers[1];
                    $yiuys= (explode('/', $rt));
                    $enddate = $yiuys[1].'-'.$yiuys[0].'-'.$yiuys[2];
                }
               
                $request['payments_unique_id'] = uniqid();
                $request['payments_category_id']=$paymentid;
                $request['create_by'] = Auth::user()->users_unique_id;
                $request['payment_status']='active';
                $request['payment_detatil'] = json_encode($result);
                $request['payment_end_date'] = $enddate;
                $request['payment_start_date'] =$startdate;
                $user = Payment::create($request->toArray());
                $useruniid =$user->toarray();
                $lastId = $useruniid['payments_unique_id'];
        
                // $paymentd = array( array( 'paymentdetail' => array('paymentdetail_unique_id' => uniqid(),'amount' => '','paydate' => '',
               
        
        
              $uniqid= [];
             $i = 0;
              $max = count($ypp);
              for($i; $i<$max; $i++):
                $oo= $max;
                        $tt = [];
                        for ($io = 1 ;$io<=$oo ;$io++) {
                            $to= array(
                                //array(
                                    
                               getIndianCurrency($io) =>  array(
                              'packageDiscription'  =>array('packageamount'=>'','countpackageamount'=>'','packagestatus'=>'','status' => '','package_payid' =>'','package_uniqueid' =>uniqid(),'packagecreater_by'=>Auth::user()->users_unique_id),
                              'paymentdetail' => array(array(
                              'pay_id' =>uniqid(),     
                              'paymentdetail_unique_id' => uniqid(),    
                              'amount' =>'',
                              'paydate' => date('yy-m-d'),
                              'status' => '',
                              'moneystatus'=>'',
                              'get' => '',
                              'create_by'=>Auth::user()->users_unique_id),
                            //   array(
                            //     'pay_id' =>uniqid(),     
                            //     'paymentdetail_unique_id' => uniqid(),    
                            //     'amount' => rand(50,30),
                            //     'paydate' => '',
                            //     'status' => '',
                            //     'moneystatus'=>'',
                            //     'get' => '',
                            //     'create_by'=>Auth::user()->users_unique_id)
                            //),
                                                   
                            ),),);
               array_push($tt, $to);
                        }
                     //print_r($tt)   ;
                 
                    $data = new addpayment;
                    $data->addpayment_unique_id = uniqid();
                    $data->addpayment_payments_unique_id = $lastId;
                    $data->addpayment_user_unique_id=$ypp[$i];
                    $data->part_payment_detatil=json_encode($tt);
                    $data->payment_status ='active';
                    $data->payment_start_date =$startdate;
                    $data->payment_start_date =$enddate;
                    $data->payment_end_date =$enddate;
                    $data->payment_amount = $dived==!NULL?($yuu[0]['amount']/$max):$yuu[0]['amount'];//(float)number_format($yuu[0]['amount']/$max, 2, '.', '')number_format($yuu[0]['amount']/$max)
                    $data->save();
                    $tts = [];
                    $yopb =[];
                    for ($io = 1 ;$io<=$oo ;$io++) {
                               $to= array(
                                    array(
                                        
                                    getIndianCurrency($io) =>  array(
                          'packageDiscription'  =>array('packageamount'=> $io==1? $dived==!NULL?($yuu[0]['amount']/$max):($yuu[0]['amount']):'' ,'countpackageamount'=>'','packagestatus'=>$io==1?'processing':'','status' => $io==1?$io:0,'package_payid' =>$data->addpayment_unique_id,'package_uniqueid' =>uniqid(),'packagecreater_by'=>Auth::user()->users_unique_id),
                          'paymentdetail' => array(array(
                          'pay_id' =>$data->addpayment_unique_id ,     
                          'paymentdetail_unique_id' => uniqid(),    
                          'amount' => '',
                          'paydate' => '',
                         // 'status' => $io==1?$io:0,
                           'status' => '',
                          'moneystatus'=>$io==1?'cash':'',
                          'get' => '',
                          'create_by'=>''),
                        //   array(
                        //     'pay_id' =>uniqid(),     
                        //     'paymentdetail_unique_id' => uniqid(),    
                        //     'amount' => rand(50,30),
                        //     'paydate' => '',
                        //     'status' => '',
                        //     'moneystatus'=>'',
                        //     'get' => '',
                        //     'create_by'=>Auth::user()->users_unique_id)
                        ),
                                               
                        ),),);
           array_push($tts, $to);
                     //
                     $go = array(array(
                             getIndianCurrency($io) =>  array(
              'packageDiscription'  =>array('packageamount'=>
                     $io==1?$dived==!NULL?($yuu[0]['amount']):($yuu[0]['amount']*$max):0,
                     'countpackageamount'=>'',
                     'packagestatus'=>$io==1?'processing':'','status' => $io==1?$io:0,'package_payid' =>$data->addpayment_unique_id,'package_uniqueid' =>uniqid(),'packagecreater_by'=>Auth::user()->users_unique_id),
              'paymentdetail' => array(array(
              'pay_id' =>$data->addpayment_unique_id ,     
              'paymentdetail_unique_id' => uniqid(),    
              'amount' => '',
              'blance' =>'',
              'get_date' => '',
              'status' => $io==1?$io:0,
              'moneystatus'=>'',
              'get_by' => '',
              'create_by'=>''),
            //   array(
            //     'pay_id' =>uniqid(),     
            //     'paymentdetail_unique_id' => uniqid(),    
            //     'amount' => rand(50,30),
            //     'paydate' => '',
            //     'status' => '',
            //     'moneystatus'=>'',
            //     'get' => '',
            //     'create_by'=>Auth::user()->users_unique_id)
            ),
                                   
            ),),);
              array_push($yopb,$go);
                     //
                    }
                     addpayment::where('addpayment_unique_id', $data->addpayment_unique_id)
                                      ->update([
                                          'part_payment_detatil' => json_encode( $tts) ]);
                    payment::where('payments_unique_id', $data->addpayment_payments_unique_id)
                                  ->update(['withdraws' => json_encode( $yopb) ]);     
                                  //sam                  
                   
            endfor;
        
            
             
             return Redirect::back()->with('Successfull message', 'Amount Added!');
            }
        }