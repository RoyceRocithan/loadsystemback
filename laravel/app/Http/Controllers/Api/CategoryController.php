<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\Category;

class CategoryController extends Controller
{
    public function Category(Request $requset){

    }
    public function AddCategory(Request $requset){
        try {

            $validator = Validator::make($request->all(), [
                'amount' => 'required',
                'users_count' => 'required',
               
            ]);

            if($validator->fails()){
                return response([
                    'error' => $validator->errors()->all()
                ], 422);
            }
            
            $request['unique_id'] = uniqid();
            $request['create_by'] = 0;
            $user = User::create($request->toArray());
            

            return response()->json([
                'status_code' => 200,
                'message' => 'Registration Successfull',
            ]);


         }catch(Exception $error){
            return response()->json([
                'status_code' => 500,
                'message' => 'Error in Registration',
                'error' => $error,
            ]);
        }

    }
}
