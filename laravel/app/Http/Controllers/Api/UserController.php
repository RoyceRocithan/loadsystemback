<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\Models\User;

class UserController extends Controller
{
    //name
    //email
    //password
    //password_confirmation
    public function index()
    {
        return "Yes";
    }
    public function register(Request $request){

        try {

            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6',
            ]);

            if($validator->fails()){
                return response([
                    'error' => $validator->errors()->all()
                ], 422);
            }
            //0 --- nomaluser
            //1--employe
            //2--superadmin
            $request['password'] = Hash::make($request['password']);
            $request['unique_id'] = uniqid();
            $request['remember_token'] = Str::random(10);
            $request['is_role'] = 0;
            $user = User::create($request->toArray());
            

            return response()->json([
                'status_code' => 200,
                'message' => 'Registration Successfull',
            ]);


         }catch(Exception $error){
            return response()->json([
                'status_code' => 500,
                'message' => 'Error in Registration',
                'error' => $error,
            ]);
        }
    }
    public function login(Request $request){
        try{
            $request->validate([
                'email' => 'email|required',
                'password' => 'required',
            ]);

            $credentials = request(['email', 'password']);

            if (!Auth::attempt($credentials)){
                return response()->json([
                    'status_code' => 422,
                    'message' => 'Unauthorized',
                    
                ]);
            }

            $user =  User::where('email', $request->email)->first();

            if(!Hash::check($request->password, $user->password, [])){
                return response()->json([
                    'status_code' => 422,
                    'message' => 'Password Match',
                    
                ]);
            }

            $tokenResult = $user->createToken('authToken')->plainTextToken;
            return response()->json([
                'status_code' => 200,
                'access_token' => $tokenResult,
                'token_type' => 'Bearer',
            ]);

        }catch(Exception $error){
            return response()->json([
                'status_code' => 500,
                'message' => 'Error in login',
                'error' => $error,
            ]);
        }
    }
    public function logout(Request $request){
        $request->user()->currentAccessToken()->delete();
        return response()->json([
                'status_code' => 200,
                'message' => 'Logout successfull',
            ]);
    }
    public function userdata(Request $request){
        // return $request->user();
        //return 'jghg';
           
            $request['unique_id'] = uniqid();
            $request['amount'] = 10;
            $request['create_by'] = 0;
            $request['users_count'] =4;
            $request['start_date'] = 01-05-2010;
            $request['end_start_date'] =03-05-2010;
            $user = User::create($request->toArray());
    }
    public function alluser(Request $request){
        $query = User::select('created_at','email','is_role','name')->get();
        return response()->json([
            'status_code' => 200,
             'data' => $query
        ]);
    }
}
