<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\expensive;
use App\Models\addpayment;
use App\Models\Orderpay;
use App\Models\Payment;
use App\Models\advacepayment;
use View;
use Redirect;
use DB;

class OrderpayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Category::all();
        $query = Category::select('*')
               ->leftJoin('orderpays', 'categories.categories_unique_id', '=', 'orderpays.orderpays_categories_unique_id')
               ->get();
        return View::make('pages.Orderpay_form',['data' => $data,'query' =>$query]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function addamountform(Request $request){
      return view::make('pages.amount_add');
    }
    public function finduser(Request $request){
        $yui = [];
        $name = $request->get('name');
        $uo = User::select('users_unique_id','name')->where('name', 'LIKE', "$name%")->get()->toArray();
        $payments = Payment::select('payment_detatil')->where('payment_status','active')->get();
        $h =  json_decode(json_encode($payments,true));
        foreach ($h as $key => $value) {
            $t = json_decode($value->payment_detatil);
            foreach ($t as $key => $value) {
                    array_push($yui,$value);
            }
        }
        $o = array_unique($yui);
        if (in_array($uo[0]['users_unique_id'], $o))
        {
          print_r('<a href="# " class="text-center" onclick="find_amount(\''.($uo[0]['name']).'\',\''.($uo[0]['users_unique_id']).'\')">'.$uo[0]['name'].'</a>');
        }
        else
        {
          echo "not found";
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $amount= $request->get('oder');
        $oderid= $request->get('oderid');
        $permistion = $request->get('oderpay');
         if((int)count($permistion) == (int)count(array_unique($permistion))){
            $uo = array_combine( $oderid, $permistion );
           
            foreach ($uo as $key => $value) {
            $query = Orderpay::where('orderpays_categories_unique_id',$key)->get();
            if($query){
              //insert
              Orderpay::insert( ['orderpays_categories_unique_id' => $key, 'oder' => $value] );  
              //return Redirect::back()->with('Successfull message', 'Amount Added!');          
            }else{
              //upodate
                Orderpay::where('orderpays_categories_unique_id', $key)
                ->update([
                    'oder' => $value ]);
                      }
                     
              } 
            return Redirect::back()->with('Successfull message', 'Amount Added!'); 
         }else{
             $errors =['Error duplicate'];
             return Redirect::back()->with($errors);
         }
        
    }
    public function addform(Request $request){
      //return $request->all();
      $query = Orderpay::select('oder')->get()->toarray();
      $to=[];
      foreach ($query as $key => $value) {
       array_push($to,$value['oder']);
      }
      sort($to);
      foreach ($to as $key => $value) {
        echo $value==3?exit:$value;
      }
    }
    public function pay(Request $request){
      return view('pages.paytest');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Orderpay  $orderpay
     * @return \Illuminate\Http\Response
     */
    public function oderpay_delete(Request $request){
          Orderpay::select('*')->delete();
        return Redirect::back()->with('Successfull message', 'Deleted!'); 
    }
    public function addacAmount(Request $request){
    
     $amount = $request->get('amount');
     $id     = $request->get('id');
     if(empty($amount)){
      return response()->json(['Error' => 'Empty error'], 200);
     }else{
         if(!is_numeric($amount)){
          return response()->json(['Error' => 'this string '], 200);
         }else{
              $request['advacepayments_unique_id']= uniqid();
              $request['advacepayment_users_unique_id']=$id;
              $request['amount'] = $amount;
              $request['date']=date('yy-m-d');
              $fgh = advacepayment::create($request->toArray());
              return response()->json(['sucess' => 'added data'], 200);
         }
     }
    }
    public function importAmount(Request $request){
      
      $data = addpayment::where('addpayment_user_unique_id',$request->get('id'))
       ->leftJoin('payments', 'payments.payments_unique_id', '=', 'addpayments.addpayment_payments_unique_id')
      ->leftJoin('categories', 'payments.payments_category_id', '=', 'categories.categories_unique_id')
      ->leftJoin('orderpays','categories.categories_unique_id','=','orderpays.orderpays_categories_unique_id')
      ->where('addpayments.payment_status', '=', 'active')
      ->get()
      ->toarray();
      
     return view('pages.replayamount',['data'=>$data,'id'=>$request->get('id')]);
    }
    public function show(Orderpay $orderpay)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Orderpay  $orderpay
     * @return \Illuminate\Http\Response
     */
    public function edit(Orderpay $orderpay)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Orderpay  $orderpay
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Orderpay $orderpay)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Orderpay  $orderpay
     * @return \Illuminate\Http\Response
     */
    public function destroy(Orderpay $orderpay)
    {
        //
    }
}
