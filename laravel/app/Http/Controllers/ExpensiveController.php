<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\expensive;
use App\Models\addpayment;
use Redirect;


class ExpensiveController extends Controller
{
    //

    public function expensive(Request $request){
        $data =expensive::leftJoin('users', 'expensives.expensives_users_unique_id', '=', 'users.users_unique_id')->get();
        return view('pages.expensive', ['data'=>$data]);
       
    }
    public function expensive_sub(Request $request){
     
        try {
  
          $validator = Validator::make($request->all(), [
              'expensives_amount' => 'required|unique:expensives|numeric|min:0',
              ]);
  
          if($validator->fails()){
              return Redirect::back()->withErrors($validator);
          }
         
          $request['expensives_users_unique_id'] = uniqid();
          $request['create_by'] = Auth::user()->users_unique_id;
        
          $user = expensive::create($request->toArray());
          
  
          return Redirect::back()->with('Successfull message', 'Amount Added!');
  
  
       }catch(Exception $error){
          return Redirect::back()->withErrors($error);
      }
      }
      public function getUserDetail($id){
        return '';
      }
      public function income(Request $request){
          $data = addpayment::all();
        return view('pages.income', ['data'=>$data]);
      }
      public function expencereport(Request $request){
        $data = expensive::select('*')->orderByRaw('updated_at - created_at DESC')->get();
      return view('pages.expencereport', ['data'=>$data]);
    }
  
}
