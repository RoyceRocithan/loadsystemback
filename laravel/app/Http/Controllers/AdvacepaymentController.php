<?php

namespace App\Http\Controllers;
use App\Models\advacepayment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\Models\User;
use App\Models\Category;
use App\Models\Payment;
use App\Models\addpayment;
use Illuminate\Support\Str;
use Redirect;
use Session;
use DB;

class AdvacepaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreadvacepaymentRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreadvacepaymentRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\advacepayment  $advacepayment
     * @return \Illuminate\Http\Response
     */
    public function show(advacepayment $advacepayment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\advacepayment  $advacepayment
     * @return \Illuminate\Http\Response
     */
    public function edit(advacepayment $advacepayment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateadvacepaymentRequest  $request
     * @param  \App\Models\advacepayment  $advacepayment
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateadvacepaymentRequest $request, advacepayment $advacepayment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\advacepayment  $advacepayment
     * @return \Illuminate\Http\Response
     */
    public function destroy(advacepayment $advacepayment)
    {
        //
    }
}
