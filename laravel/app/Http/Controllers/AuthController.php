<?php

namespace App\Http\Controllers;
use App\Models\Category;
use App\Models\Payment;
use App\Models\addpayment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\Models\User;
use Illuminate\Support\Str;
use Redirect;
use Session;
use File;
use Illuminate\Validation\Rule;

class AuthController extends Controller
{
    public function Authication_form(Request $request){
         return View::make('pages.login');
     
    }
    public function Authication_forget_form(Request $request){
        return View::make('pages.forgotpassword');
    
   }
   public function deletemember(Request $request){
    $fg = array();
    $payment = Payment::select('payment_detatil')->where('payment_status','active')->get()->toArray();
    foreach ($payment as $key => $value) {
        $yrt = (array)$value['payment_detatil'];
        foreach ($yrt as $key => $value) {
            $yt= json_decode($value,true);
          foreach ($yt as $key => $valued) {
            // print_r($valued);
             array_push($fg,$valued);
          }
        }
    }
   
    $y = array_unique($fg);
    if (in_array($request->get('id'), $y)) {
        return response()->json(['Error' => 'This user activated in package'], 200);
         }
    else {
        $fgs = array();
        $payments = Payment::select('payment_detatil')->where('payment_status','inactive')->get()->toArray();
        foreach ($payments as $key => $value) {
            $yrts = (array)$value['payment_detatil'];
            foreach ($yrts as $key => $value) {
                $yts= json_decode($value,true);
              foreach ($yts as $key => $valued) {
                array_push($fgs,$valued);
              }
            }
        }
       
        $yo = array_unique($fgs); 
        // if (in_array($request->get('id'), $yo)) {}
        // echo "not found";
        if (in_array($request->get('id'), $yo)){
           // echo "found";
           User::where('users_unique_id', $request->get('id'))
           ->update([
                     'delete_permition' =>1,
                   ]);
              return response()->json(['Sucesses' => 'moved Recycle.. '], 200);       
           }
            else
            {
                $res=User::where('users_unique_id',$request->get('id'))->delete();
                return response()->json(['Sucesses' => 'deleted'], 200);
            }
     }
    
   }
   public function Authtication(Request $request){
    // $request->validate([
    //     'email' => ['required', 'string', 'email', 'max:191',Rule::unique('users')->where(function ($query) use ($request) {
    //         return $query->where('is_role', 0);
    //     })],
    //     'password' => 'required',
    // ]);
    $credentials = $request->only('email', 'password');
    if (Auth::attempt($credentials)) {
        if(Auth::user()->is_role==2){
            return redirect()->intended('dashboard')
                         ->withSuccess('Logged-in');
        }elseif(Auth::user()->is_role==0){
            return redirect()->intended('userdashboard')
                         ->withSuccess('Logged-in');
        }
        
    }
    return redirect("login")->with('error message', 'Username or password incorrect');
    
   }
   public function member(){
       $member = User::all();
       return View::make('pages.member',['member' => $member]);
   }
   public function editmember(request $request ){
     return redirect('pages.editmember');   
   
   }
   public function restoremember(request $request){
       User::where('users_unique_id', $request->get('id'))
       ->update([
                 'delete_permition' =>0,
                 ]);
    return response()->json(['Sucess' => 'Member Restored'],200);
   }
   public function updatemember(request $request){
      // return $request->all();
    //    $len = strlen($request->get('Ic_number'));
    //    if($len!==10 || $len!==12){
    //      return 'dfh';
    //    }else{
    //        return 'tuyt';
    //    }
      
     if($request->file('image')!==NULL)  {

       

                if($request->get('imgname')!==NULL){
                $result = explode('\\', $request->imgname);
                $filepath =$result[0];
                $filename =$result[1];
                $image_path =$result[0].'/'.$result[1];
                        if(File::exists($image_path)) {
                            File::delete($image_path);
                        }
                       
            }
            $imageName = time().'.'.$request->file('image')->extension();  
            $t=$request->file('image')->move(('images'), $imageName);
            
            User::where('id', $request->get('id'))
              ->update([
                        'name' =>$request->get('name'),
                        'email' =>$request->get('email'),
                        'Shop_name'=>$request->get('Shop_name'),
                        'Phonenumber'=>$request->get('Phonenumber'),
                        'Ic_number'=>$request->get('Ic_number'),
                        'Profile_picture'=>$t,
                        'password'=>Hash::make($request->get('password')),
                    ]);

     }
         User::where('id', $request->get('id'))
              ->update([
                        'name' =>$request->get('name'),
                        'email' =>$request->get('email'),
                        'Shop_name'=>$request->get('Shop_name'),
                        'Phonenumber'=>$request->get('Phonenumber'),
                        'Ic_number'=>$request->get('Ic_number'),
                        //'Profile_picture'=>$t,
                        'password'=>Hash::make($request->get('password')),
                    ]);
                    return response()->json(['Sucess' => 'Member Updated'],200);
   }
   public function logout()
   {
       Session::flush();
       
       Auth::logout();

       return redirect('login');
   }
   public function register(Request $request){

    try {

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'Phonenumber' => 'regex:/(0)[0-9]{9}/',
            'Ic_number'  => 'required'//|regex:/[0-9]{12}/
            ]);

            if($validator->fails()){
                return Redirect::back()->withErrors($validator);
            }
        //0 --- nomaluser
        //1--employe
        //2--superadmin
         if($request->image==!null){
              $imageName = time().'.'.$request->image->extension();  
              $t=$request->image->move(('images'), $imageName);
         }
       

        $request['password'] = Hash::make($request->get('password'));
        $request['users_unique_id'] = uniqid();
        $request['remember_token'] = Str::random(10);
        $request['is_role'] = 0;
        $request['delete_permition']=0;
        $request['Profile_picture'] = $request->image==''?"":$t;
        $user = User::create($request->toArray());
       
        

        // return response()->json([
        //     'status_code' => 200,
        //     'message' => 'Registration Successfull',
        // ]);
        return Redirect::back()->with('Successfull message', 'User Detail Registered!');


     }catch(Exception $error){
        // return response()->json([
        //     'status_code' => 500,
        //     'message' => 'Error in Registration',
        //     'error' => $error,
        // ]);
        return Redirect::back()->withErrors($error);
    }
}
}
/**INSERT INTO `users` (`id`, `users_unique_id`, `name`, `is_role`, `email`, `Shop_name`, `Phonenumber`, `Ic_number`, `Profile_picture`, `delete_permition`, `email_verified_at`, `password`, `deleted_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(53, '622f34fadae82', 'shyamalan', '2', 'shya@gamil.com', 'shya', '0771369472', '49', NULL, '0', NULL, '$2y$10$BSA9zZyAMIGt7ikZ9n1RHeuzOk3Em.FRY2iCeT0goEKvaGvgZ8Q1O', NULL, NULL, '2022-03-14 06:58:42', '2022-03-15 21:20:01');
 */