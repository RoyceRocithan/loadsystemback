<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\Models\User;
use Illuminate\Support\Str;
use Redirect;
use Session;
use Illuminate\Validation\Rule;

class UserReportControllerName extends Controller
{
    public function index(Request $request){
        return View::make('pages.userdashboard');
    }
}
