<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\Models\User;
use App\Models\Category;
use App\Models\Payment;
use App\Models\addpayment;
use App\Models\expensive;
use Illuminate\Support\Str;
use Redirect;
use Session;
use DB;

class DashboardController extends Controller
{
    public function Dashboard(Request $request){
       $totaluser = User::select('id')->count();
       $totalcat = Category::select('id')->count();
       $expensive = expensive::select('expensives_amount')->where('created_at', '>=', date('Y-m-d').' 00:00:00')->get();
       $amount = [];
       $total = [];
        $name = DB::table('addpayments')->select('part_payment_detatil')->get()->toArray();
         foreach ($name as $key => $blog) {
          foreach ((object)$blog as  $value) {
                $i= json_decode( $value);
               foreach ($i as $key => $value2) {
                     foreach ($value2 as $key => $value3) {
                      $t = json_decode(json_encode($value3), true);
                     foreach ($value3 as $key => $value4) {
                       array_push($amount,$value4->paymentdetail);
                       }
                  }
               }
            
          }
         }
         foreach ($amount as $key => $value) {
           foreach ($value as $key => $value1) {
              array_push($total,(int)$value1->amount);
           }
         }
                
     return View::make('pages.dashboard',['totaluser' =>$totaluser,'expensive'=>$expensive,'totalcat' =>$totalcat,'totalamount' => array_sum($total)]);
    }
    // public function Dashboard(Request $request){
       
    //     $yoo = [];
    //     $tof= [];
    //     $name = DB::table('addpayments')->select('part_payment_detatil')->where('addpayment_unique_id','=','61df92551726b')->get()->toArray();
    //     $ys = json_decode(json_encode($name), true);
    //       foreach ($ys as $key => $value1) {
    //        foreach ($value1 as $key => $values) {
    //           $y = json_decode($values);
    //         foreach ($y as $key => $valuesr) {
    //             //$this->test($valuesr);
    //             $this->slll($valuesr);
    //             // array_push($yoo,$valuesr);
    //              array_push($tof,$valuesr);
    //         }
    //        }
    //     }
    //    print('<pre>');
    //    print_r($tof); 
    //    print('</pre>');
    //   //  addpayment::where('id', 371)
    //   //  ->update([
    //   //      'part_payment_detatil' => json_encode($tof)
    //   //     ]) ;
     
    // }
    // public function slll($t){
           
                 
    //           //--------------------------------------------------------------------------------
    //             foreach ($t as $key => $values) {
                    
    //                     foreach ($values as $key => $valueer) {
    //                             foreach ($valueer->paymentdetail as $key => $valueee) {
                                 
    //                       //$valueee->paymentdetail_unique_id=='61df925518032'?$valueee->amount='messi':false;
    //                           if($valueee->paymentdetail_unique_id=='61df925518032'){
    //                             unset($valueer->paymentdetail[$key]);
    //                           }else{
    //                               return false;
    //                           }
    //                         }
                            
    //                     }
    //             }
    //             return $t;
    // }
    // public function test($uo){
    //     $array =array(
    //              'pay_id' =>78000,     
    //              'paymentdetail_unique_id' => uniqid(),    
    //              'amount' =>500,
    //              'paydate' => '',
    //              'status' => '',
    //              'moneystatus'=>'',
    //              'get' => '',
    //              'create_by'=>Auth::user()->users_unique_id
    //     );
    //     // print('<pre>');
    //     // print_r($uo);
    //     // print('</pre>');
    //     $ut = array_column($uo,'one');
    //     foreach ($ut as $key => $value) {
    //      array_push($value->paymentdetail,$array);
    //      }
    //      return $uo;
    //     // print('<pre>');
    //     // print_r($uo);
    //     // print('</pre>'); 
    // }
  public function round(){
    $payment = Payment::select('*')
    ->leftJoin('users', 'payments.create_by', '=', 'users.users_unique_id')
    ->leftJoin('addpayments', 'addpayments.addpayment_unique_id', '=', 'payments.payments_unique_id')
    ->leftJoin('categories', 'payments.payments_category_id', '=', 'categories.categories_unique_id')
    ->where('Payments.payment_status', '=', 'active')
    
    ->get();
    return View::make('pages.add_round',['payment'=>$payment]);
  }
}
