<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\UserController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
 Route::middleware(['auth:sanctum','roleChecker'])->group(function () {
   Route::get('/user',[UserController::class,'userdata']);  
   Route::get('/logout',[UserController::class,'logout']);
   
 });
 Route::middleware(['auth:sanctum','superAdmin'])->group(function () {
    Route::get('/superAdmin',[UserController::class,'userdata']); 
    Route::get('/logout',[UserController::class,'logout']); 
  });
  Route::middleware(['auth:sanctum','employee'])->group(function () {
    Route::get('/employee',[UserController::class,'userdata']); 
    Route::get('/logout',[UserController::class,'logout']); 
  });


Route::post('/register',[UserController::class,'register']);
Route::post('/login',[UserController::class,'login']);
Route::get('/alluser',[UserController::class,'alluser']); 


