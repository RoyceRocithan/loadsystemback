<?php

use Illuminate\Support\Facades\Route;
use App\Events\MessageCreated;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\paymentController;
use App\Http\Controllers\AddpaymentController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ExpensiveController;
use App\Http\Controllers\UserReportControllerName;
use App\Http\Controllers\OrderpayController;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('message/created', function () {
  MessageCreated::dispatch('shyamalan as a developer');  
});
Route::get('/test', function () {
    
  return view('test');
});
Route::get('contact', function()
{
    return View::make('pages.replay');
});

Route::get('profile', function()
{
    return View::make('pages.profile');
});


Route::get('/login', [AuthController::class, 'Authication_form']);
Route::post('/authcheck', [AuthController::class, 'Authtication']);
Route::get('/forgot', [AuthController::class, 'Authication_forget_form']);
//Route::get('/member', [AuthController::class, 'member']);
Route::get('/logout', [AuthController::class, 'logout']);
//register
// Route::post('/registersub', [AuthController::class, 'register']);
// Route::get('/Add_payment', [AddpaymentController::class, 'Add_payment_form']);
// Route::post('/findname', [AddpaymentController::class, 'findname']);
// Route::post('/UpdateAmount',[AddpaymentController::class, 'UpdateAmount']);
// Route::post('/ReturnAmount',[AddpaymentController::class,'ReturnAmount']);
// Route::get('/addround', function () {
    
//   return view('pages.add_round');
// });
// Route::middleware(['roleChecker'])->group(function () {
   
  Route::get('/', function(){ return View::make('pages.login');});
        
    
//   });
  // Route::middleware(['superAdmin'])->group(function () {
  //   Route::get('/dashboard',[DashboardController::class, 'dashboard']);
  //  });
//    Route::middleware(['employee'])->group(function () {
     
//    });Add_payment,AddpaymentController
// Route::group(['middleware' => [ 'employee']], function () {
//     Route::get('/dashboard',[DashboardController::class, 'dashboard']);
//     Route::get('profile', function(){ return View::make('pages.profile');});
//    // Route::get('caterories', function(){ return View::make('pages.caterories');});
//     //Route::get('round/{id}', [CategoryController::class, 'getUserDetail']);
//      Route::get('caterories', [CategoryController::class, 'caterories'] );
//     Route::post('/cateories_sub', [CategoryController::class, 'cateories_sub']);
//     Route::post('/partAmount',[AddpaymentController::class,'partAmount']);
//     Route::get('/Add_cat_money_form', [paymentController::class, 'Add_cat_money_form']);
//     Route::post('/Add_cat_money_sub', [paymentController::class, 'Add_cat_money_sub']);
//     Route::post('DeleteAmount',[AddpaymentController::class,'DeleteAmount'] );
//     Route::post('roundadd',[AddpaymentController::class,'Roundadd'] );//withdorw
//     Route::post('withdorw',[AddpaymentController::class,'withdorw'] );
//     Route::post('/CashAmount', [AddpaymentController::class,'CashAmount']);
//     Route::get('expensive', [ExpensiveController::class, 'expensive'] );
//     Route::post('/expensive_sub', [ExpensiveController::class, 'expensive_sub']);
//    });

// Route::get('/round',[DashboardController::class, 'round']);

Route::group(
	[
		
		'middleware' => ['App\Http\Middleware\superAdmin']
		
	],
	function()
	 {	
    Route::get('/dashboard',[DashboardController::class, 'dashboard']);
    Route::get('profile', function(){ return View::make('pages.profile');});
    Route::get('caterories', [CategoryController::class, 'caterories'] );
    Route::post('/cateories_sub', [CategoryController::class, 'cateories_sub']);
    Route::post('/partAmount',[AddpaymentController::class,'partAmount']);
    Route::get('/Add_cat_money_form', [paymentController::class, 'Add_cat_money_form']);
    Route::post('/Add_cat_money_sub', [paymentController::class, 'Add_cat_money_sub']);
    Route::post('DeleteAmount',[AddpaymentController::class,'DeleteAmount'] );
    Route::post('roundadd',[AddpaymentController::class,'Roundadd'] );//withdorw
    Route::post('withdorw',[AddpaymentController::class,'withdorw'] );
    Route::post('/CashAmount', [AddpaymentController::class,'CashAmount']);
    Route::get('expensive', [ExpensiveController::class, 'expensive'] );
    Route::get('income', [ExpensiveController::class, 'income'] );
    Route::get('expencereport', [ExpensiveController::class, 'expencereport'] );
    Route::get('oderform', [OrderpayController::class, 'index'] );
    Route::get('amount_add', [OrderpayController::class, 'addamountform'] );
    Route::post('oderpay_sub',[OrderpayController::class, 'store']);
    Route::post('oderpay_delete',[OrderpayController::class, 'oderpay_delete']);
    Route::get('/pay',[OrderpayController::class, 'pay']);
    Route::post('finduser',[OrderpayController::class, 'finduser']);
    Route::post('importAmount',[OrderpayController::class, 'importAmount']);
    Route::post('addform',[OrderpayController::class, 'addform']);
    Route::post('addacAmount',[OrderpayController::class, 'addacAmount']);
    
    //Userview
    Route::post('Userview', [CategoryController::class, 'Userview'] );
    Route::post('/expensive_sub', [ExpensiveController::class, 'expensive_sub']);
    Route::get('/round',[DashboardController::class, 'round']);
    Route::get('/forgot', [AuthController::class, 'Authication_forget_form']);
    Route::get('/member', [AuthController::class, 'member']);
    Route::get('edit-member', [AuthController::class, 'editmember']);
    Route::post('updatemember', [AuthController::class, 'updatemember']);
    Route::post('deletemember', [AuthController::class, 'deletemember']);
    Route::post('/restoremember', [AuthController::class, 'restoremember']);
    Route::post('/registersub', [AuthController::class, 'register']);
    Route::get('/Add_payment', [AddpaymentController::class, 'Add_payment_form']);
    Route::post('/findname', [AddpaymentController::class, 'findname']);
		}
);

Route::group(
	[
		
		'middleware' => ['App\Http\Middleware\employee']
		
	],
	function()
	 {	
    //Route::get('userdashboard', function() { return View::make('pages.userdashboard');});
    Route::get('/userdashboard',[UserReportControllerName::class, 'index']);
    Route::get('/userprofile', function(){ return View::make('pages.userprofile');});
    
		}
);