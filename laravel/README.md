<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About Laravel
$2y$10$b.BvvyceMnhh1I69/EloVuGCySxvILwIT8AZiXN/GTWEojwGqrA1K

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for funding Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](https://patreon.com/taylorotwell).

### Premium Partners

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**
- **[64 Robots](https://64robots.com)**
- **[Cubet Techno Labs](https://cubettech.com)**
- **[Cyber-Duck](https://cyber-duck.co.uk)**
- **[Many](https://www.many.co.uk)**
- **[Webdock, Fast VPS Hosting](https://www.webdock.io/en)**
- **[DevSquad](https://devsquad.com)**
- **[Curotec](https://www.curotec.com/services/technologies/laravel/)**
- **[OP.GG](https://op.gg)**
- **[CMS Max](https://www.cmsmax.com/)**
- **[WebReinvent](https://webreinvent.com/?utm_source=laravel&utm_medium=github&utm_campaign=patreon-sponsors)**
- **[Lendio](https://lendio.com)**
- **[Romega Software](https://romegasoftware.com)**

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
https://bootstrap-vue.js.org/
https://www.antdv.com/components/mentions/
https://playcode.io/
https://1337x.to/torrent/4442020/The-Complete-Flutter-UI-Masterclass-IOS-Android-In-Dart-Udemy-Course/
https://jsonplaceholder.typicode.com/photos
https://www.youtube.com/c/SanskarTiwari/playlists
https://codesandbox.io/search
https://snack.expo.io/
https://surge.sh/help/
https://reactrouter.com/web/guides/quick-start
https://www.codegrepper.com/code-examples/php/laravel+get+list+of+columns+in+a+table
https://stackblitz.com/edit/react
https://www.youtube.com/playlist?list=PLgMICEduGwEzy6jqbR_yciKiGDsto74Dq

 <img src="<?=Auth::user()->Profile_picture==''?'AdminLTE-master/dist/img/user2-160x160.jpg':Auth::user()->Profile_picture?>" width="160" height="160" class="img-circle elevation-2" alt="User Image">
 <a href="/profile" class="d-block"><?=Auth::user()->name?></a>
 $2y$10$bY4RqPTXDC7uDsFFBaJ6eOKJm6oBQCBkpDYmblvY15U9UXmkBj7fi

 INSERT INTO `users` (`id`, `users_unique_id`, `name`, `is_role`, `email`, `Shop_name`, `Phonenumber`, `Ic_number`, `Profile_picture`, `email_verified_at`, `password`, `deleted_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(7, '61c2afc8e2885', 'shyamalan', '1', 'shyamalan19876@gmail.com', 'shyamalan', '0712124168', '861513971V', 'images\\1640148936.jpg', NULL, '$2y$10$bY4RqPTXDC7uDsFFBaJ6eOKJm6oBQCBkpDYmblvY15U9UXmkBj7fi', NULL, NULL, '2021-12-21 23:25:36', '2021-12-21 23:25:36');

pirathi
500000/50

2000*50=100000
100000-85000 ---1
85000/50=1700
100000-80000 ----2
80000/50=1600
100000-90000----3
90000/50=1800
//////////////////////////////////////
2000*5=10000
10000-8500----w(1)
8500/5=1700----par
10000-8000---2
8000/5=1600
10000-9000----3
9000/5=1800
10000-7500----4
7500/5=1500
10000-9000----5



clos 