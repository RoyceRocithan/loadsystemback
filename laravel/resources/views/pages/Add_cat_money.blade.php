<!doctype html>
<html>
<head>
    @include('includes.head')
    @php
    use App\Models\User;
    
    @endphp
    <style>
      .list-group{
    max-height: 300px;
    margin-bottom: 10px;
    overflow:scroll;
    -webkit-overflow-scrolling: touch;
      }
      .crossover-box {
    color: #5A53EA;
    height: 300px !important;
}
    </style>
 <script>
  
//  function retest(){
//   var o =$('#multiple :selected').toArray().map(obj => {
//    let rObj = {}
//    rObj[obj.value] = obj.text
//     $('#mySelectttt').append( '<option value="'+obj.value+'">'+obj.text+'</option>' );
// })
//  }
 </script>
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

    <header class="row">
        @include('includes.header')
    </header>
@include('includes.Sidebar')
    {{-- <div id="main" class="row">

            @yield('content')

    </div> --}}
    <div class="content-wrapper">
        @include('includes.contentheader')
        <div class="d-flex justify-content-center">
         <section class="content col-md-8 col-sm-12 ">
            <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">pay-add-Amount</h3>
                  
                </div> 
               
               
                <div class="card card-default">
                    <div class="card-header">
                      <h3 class="card-title"></h3>
          
                      <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                          <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                          <i class="fas fa-times"></i>
                        </button>
                      </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                      <div class="row">
                        <div class="col-12">
                          <form action="{{url('Add_cat_money_sub')}}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                          <div class="form-group">
                            <label>Amount</label>
                            <select class="form-control select2" name="category_id" style="width: 100%;"> 
                              <option value="">--select--</option>
                              @foreach($cat as $cate)
                                <option value="{{ $cate->categories_unique_id }}">
                                {{ $cate->amount }}.00
                               </option>
                               @endforeach

                            </select>
                          </div>
                          <div class="form-group">
                                  <div class="icheck-primary d-inline">
                                  <input type="checkbox" name="Divide" id="checkboxPrimary2"><label for="checkboxPrimary2">Divide</label>
                                  </div>
                          </div>        
                          <div class="form-group">
                            <label>Add Names</label>
                            <select class="duallistbox" id="multiple" name="payment_detatil[]" required multiple="multiple">
                              
                              @foreach($users as $user)
                                <option onclick="removename()" value="{{ $user->users_unique_id }}" >
                                {{ $user->name }}
                               </option>
                               @endforeach 
                            </select>
                          </div>
                          <!-- /.form-group -->
                           <!-- /.form group -->
                <!-- Date range -->
                <div class="form-group">
                    <label>Date range:</label>
  
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="far fa-calendar-alt"></i>
                        </span>
                      </div>
                      <input type="text" name="payment_start_date"  class="form-control float-right" id="reservation">MM-DD-YYYY
                    </div>
                    <!-- /.input group -->
                  </div>
                  <div class="from-group">
                   <a class="btn btn-primary m-1" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" onclick="test()" id="Add_Duplicate" aria-controls="collapseExample">
                      <svg  xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus" viewBox="0 0 16 16">
                        <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"></path>  </svg>
                    </a>
                   <div class="collapse" id="collapseExample">
                      <div  class="form-group">
                         
                         <div class="form-group">
                          <label for="exampleFormControlSelect2">Duplicate Member list</label>
                          <select multiple class="form-control" name="duplicate[]" id="selectedduplicate">
                          
                          </select>
                        </div>
                        
                      </div>
                      <div class="form-group">
                        
                        <label>Member list</label>
                        <input class="form-control" id="myInput" name="search" type="text" placeholder="Search..">
                        <ul class="list-group" id="myList">
                         
                        </ul> 
                          </div>
                    </div>
                  </div>
                  <button type="submit" class="btn btn-primary">Submit</button>
                </form>
                  <!-- /.form group -->
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                      
                      @if($errors->any())
            <div class="alert top-2 alert-danger alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              {{ implode('', $errors->all()) }}
               </div>
              @endif
             
              @if(session()->has('Successfull message'))
              <div class="alert top-2 alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  {{ session()->get('Successfull message') }}
              </div>
            @endif
                    </div>
                  </div>
                    <!-- /.form-group -->
                  
             
            
            <div class="card card-primary m-2">
                <div class="card-header ">
                  <h3 class="card-title">DataTable with default features</h3>
                </div>
                {{-- {{$payment}} --}}
                <!-- /.card-header -->
                <div class="card-body">
                
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>Name</th>
                      <th>Amount</th>
                      
                      <th>CreateBy</th>
                      <th>tot/User</th>
                      <th>amout-of</th>
                      <th>Status</th>
                      <th></th>
                    </tr>
                    </thead>
                    <tbody>
                         @foreach ($payment as $item)
                          <tr>
                            <td>{{$item->name}}</td>
                            <td>{{$item->amount}}</td>
                             <td>{{$item->name}}</td>
                            <td>{{ count( json_decode($item->payment_detatil)) }}</td>
                            <td>{{$item->Divide==='on'?(int)$item->amount /count( json_decode($item->payment_detatil)):($item->amount *count( json_decode($item->payment_detatil)) ) }}</td>
                            <td>{{$item->payment_status==='active'?'Incomplete':'Complete'}}</td>
                            <td>
                              <button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash text-danger"></i></button>
                              <button type="button" class="btn btn-default btn-sm m-1" data-id="{{$item->payments_unique_id}}"  onclick="userview(this)"><i class="fa fa-edit text-primary"></i></button>
                                                            
                            </td>
                          </tr>
                      @endforeach
                    </tbody>
                  
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!--datatable-->
           </div>
            </div>   </div>
        </section>
        </div>  
       
    </div> 
    @include('includes.userviewmodel');

    <footer class="row">
        @include('includes.footer')
        @include('includes.filter')
        @include('includes.datatable')
        
    </footer>

</div>
</body>
 <script>
    function userview(e){
      // console.log(e.getAttribute("data-id"));
      //$('#useridview').modal('show');
      var data = {unque:e.getAttribute("data-id")};
      $.ajax({
          cache: false, 
          type: "POST",
          async: true,
          url: "/Userview",
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          data: data,
          dataType: "json",
          success: function(data){
           
            $('#replayuser').html(data.data);
             $('#useridview').modal('show');
          }
      });
    }
   $(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myList li").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
 $('#Add_Duplicate').click(function(e){
   //console.log('fhfg');
 });
//  $('.moveall').click(function(){
//    console.log('ghj');
//   $('#collapseExample').collapse('hide');
//  });

 function test(){
 
 $('#collapseExample').collapse('hide');
    $("#selectedduplicate option").remove();
   var filters =$('#multiple :selected').toArray().map(obj => {
   let rObj = {}
   rObj[obj.value] = obj.text
  // $("#mySelectttt option[value='"+obj.value+"']").remove();
   return rObj;
})

for (var i = 0; i < filters.length; i++) { // the plainest of array loops
  var obj = filters[i];
  // for..in object iteration will set the key for each pair
  // and the value is in obj[key]
  for (var key in obj) { 
    console.log(key, obj[key])
     $("#myList").append('<li onclick="member(\''+key+'\',\''+obj[key]+'\')">'+obj[key]+'</li>');                  
  }
}

 }
 function moveall(){
  $('#myList li').remove();
  $("#selectedduplicate option").remove();
  $('#collapseExample').collapse('hide');
 }
 function removeall(){
  $('#myList li').remove();
  $("#selectedduplicate option").remove();
  $('#collapseExample').collapse('hide');
 }
 function member(keyname,value){
   $('#selectedduplicate')
          .append($("<option selected></option>")
                     .attr("value", keyname)
                     .text(value));
 }
 function removename(){
   console.log('fghfg');
 }

 </script>
</html>