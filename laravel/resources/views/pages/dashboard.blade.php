<!doctype html>
<html>
<head>
    @include('includes.head')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
        <?php 
         use Illuminate\Support\Facades\Auth;
         use App\Models\addpayment;
         use App\Models\User;
        use App\Models\Category;
        use App\Models\Payment;

        ?>
    <header class="row">
        @include('includes.header')
    </header>
@include('includes.Sidebar')
    {{-- <div id="main" class="row">

            @yield('content')

    </div> --}}
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1><?=Route::getFacadeRoot()->current()->uri();?></h1>
              
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/logout">logout</a></li>
                {{-- <li class="breadcrumb-item active">Project Detail</li> --}}
              </ol>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>
  
      <!-- Main content -->
      <section class="content">

        <!-- Default box -->
        <div class="card">
        <div class="card-header">
          <h3 class="card-title">Income Report Detail</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
          <a class="ml-auto" href="/income">More</a>
        </div>
        </div>

         <!-- Default box -->
         <div class="card">
          <div class="card-header">
            <h3 class="card-title">Expencive Report Detail</h3>
  
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
              </button>
            </div>
          </div>
          <div class="card-body">
            @foreach ($expensive as $item)
              {{$item->expensives_amount}}.00<br>  
            @endforeach
          </div>
           <a class=" ml-auto" href="/expencereport">More</a>
         </div>
             
  
        <!-- Default box -->
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Projects Detail</h3>
  
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
              </button>
            </div>
          </div>
          <div class="card-body">
             <div class="row">
              <div class="col-12 col-md-12 col-lg-8 order-2 order-md-1">
                <div class="row">
                  <div class="col-12 col-sm-4">
                    <div class="info-box bg-light">
                      <div class="info-box-content">
                        <span class="info-box-text text-center text-muted">Customer</span>
                        <span class="info-box-number text-center text-muted mb-0">{{$totaluser}}</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-sm-4">
                    <div class="info-box bg-light">
                      <div class="info-box-content">
                        <span class="info-box-text text-center text-muted">Cat</span>
                        <span class="info-box-number text-center text-muted mb-0">{{$totalcat}}</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-sm-4">
                    <div class="info-box bg-light">
                      <div class="info-box-content">
                        <span class="info-box-text text-center text-muted">Total payment</span>
                        <span class="info-box-number text-center text-muted mb-0">{{$totalamount}}</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12">
                    <h4>Recent Payment Activity</h4>
                    <?php 
                    
                    $amount = [];
                    $ut = [];
                      $name = DB::table('addpayments')->select('part_payment_detatil')->where('payment_status','=','active')->get()->toArray();
                      
                      
                           foreach ($name as $key => $blog) {
                        foreach ((object)$blog as  $value) {
                              $i= json_decode( $value);
                            foreach ($i as $key => $value2) {
                               foreach ($value2 as $key => $value3) {
                                  $yo = json_encode($value3);
                                  $yuiyu =json_decode($yo);
                                  $q = (array)$yuiyu;
                                  foreach ($q as $key => $value) {
                                                                  
                                    foreach ($value->paymentdetail as $key => $value) {
                                      array_push($ut,$value);
                                    }
                                  
                                  }
                                }
                            }
                          
                        }
                      
                  }
                  
                    function cmp($ut,$b){
                        return strtotime($ut->paydate==NULL?'':$ut->paydate)<strtotime($b->paydate==NULL?'':$b->paydate)?1:-1;
                    };
                    uasort($ut,'cmp');
                     //print_r($ut);
                   
             
                ?>
                <?php 
                 foreach (array_slice($ut, 0, 4) as $key => $value) {
                    //  print_r($value->pay_id);
                    // }
                ?>
                      <div class="post">
                        <div class="user-block">
                            <?php 
                               $query1 = addpayment::where('addpayment_unique_id','=',$value->pay_id)
                               ->leftJoin('users', 'addpayments.addpayment_user_unique_id', '=', 'users.users_unique_id')
                               ->leftJoin('payments', 'addpayments.addpayment_user_unique_id', '=', 'payments.payments_unique_id')
                               //->leftJoin('categories', 'payments.payments_category_id', '=', 'categories.categories_unique_id')
                               ->get();
                                                           
                          ?>
                          <img class="img-circle img-bordered-sm" src="<?=$query1[0]['Profile_picture']==''?'AdminLTE-master/dist/img/user2-160x160.jpg'
                          :$query1[0]['Profile_picture']?>" width="160" height="160" alt="user image">
                          <span class="username">
                            <a href="#"><?=$query1[0]['name']?>.</a>
                          </span>
                          <span class="description">paid Date for - <?php echo $value->paydate?></span>
                        </div>
                         Invice Bill Amount <?=$query1[0]['payment_amount']?> Paid Amount
                         <?=$value->amount?>
                        <!-- /.user-block -->
                       
                        {{-- <p>
                          Lorem ipsum represents a long-held tradition for designers,
                          typographers and the like. Some people hate it and argue for
                          its demise, but others ignore.
                        </p> --}}
                        <p></p>
  
                        <p>
                          {{-- <a href="#" class="link-black text-sm"><i class="fas fa-link mr-1"></i> Demo File 1 v2</a> --}}
                          <a href="#" class="link-black text-sm"><i class="fas fa-link mr-1"></i> </a>
                        </p>
                      </div>
                      <?php 
                             }
                      ?>
  
                     
                  </div>
                </div>
              </div>
               
              {{-- <div class="col-12 col-md-12 col-lg-4 order-1 order-md-2">
                <h3 class="text-primary"><i class="fas fa-paint-brush"></i> Recent all activte</h3>
                <p class="text-muted">Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua butcher retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, qui irure terr.</p>
                <br>
                <div class="text-muted">
                  <p class="text-sm">Client Company
                    <b class="d-block">Deveint Inc</b>
                  </p>
                  <p class="text-sm">Project Leader
                    <b class="d-block">Tony Chicken</b>
                  </p>
                </div>
  
                <h5 class="mt-5 text-muted">Project files</h5>
                <ul class="list-unstyled">
                  <li>
                    <a href="" class="btn-link text-secondary"><i class="far fa-fw fa-file-word"></i> Functional-requirements.docx</a>
                  </li>
                  <li>
                    <a href="" class="btn-link text-secondary"><i class="far fa-fw fa-file-pdf"></i> UAT.pdf</a>
                  </li>
                  <li>
                    <a href="" class="btn-link text-secondary"><i class="far fa-fw fa-envelope"></i> Email-from-flatbal.mln</a>
                  </li>
                  <li>
                    <a href="" class="btn-link text-secondary"><i class="far fa-fw fa-image "></i> Logo.png</a>
                  </li>
                  <li>
                    <a href="" class="btn-link text-secondary"><i class="far fa-fw fa-file-word"></i> Contract-10_12_2014.docx</a>
                  </li>
                </ul>
                <div class="text-center mt-5 mb-3">
                  <a href="#" class="btn btn-sm btn-primary">Add files</a>
                  <a href="#" class="btn btn-sm btn-warning">Report contact</a>
                </div>
              </div> --}}
              
            </div>
            
            
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
         
      </section>
      <!-- /.content -->
    </div>
     

    <footer class="row">
        @include('includes.footer')
    </footer>

</div>
</body>
</html>