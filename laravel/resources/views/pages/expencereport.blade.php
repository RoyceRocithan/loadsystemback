<!doctype html>
<html>
<head>
    @include('includes.head')
    @php
    use App\Models\User;
    
    @endphp

</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

    <header class="row">
        @include('includes.header')
    </header>
@include('includes.Sidebar')
    {{-- <div id="main" class="row">

            @yield('content')

    </div> --}}
    <div class="content-wrapper">
        @include('includes.contentheader')
        <div class="d-flex justify-content-center">
         <section class="content col-md-8 col-sm-12 ">
          <div class="card-body">
               <div class="card-body">
                   <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>Amount</th>
                      <th>Date</th>
                      <th>CreateBy</th>
                      <th>Discription</th>
                    </tr>
                    </thead>
                    <tbody>
                      @foreach ($data as $item)
                      <tr>
                      <td>{{$item->expensives_amount}}</td>
                      <td>{{date('d-m-Y', strtotime($item->created_at))}}</td>
                      <td>{{$item->expensives_date}}</td>
                      <td>{{$item->Discription}}</td>
                     
                    </tr> 
                      @endforeach
                    
                   
                    
                    </tbody>
                  
                  </table> 
                </div>
                <!-- /.card-body -->
              </div>
              <!--datatable-->
           </div>
            </div>   </div>
        </section>
        </div>  
       
    </div> 

    <footer class="row">
        @include('includes.footer')
        @include('includes.datatable')
    </footer>

</div>
</body>
 <script>
 
 </script>
</html>