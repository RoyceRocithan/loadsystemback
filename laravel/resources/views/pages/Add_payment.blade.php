<!doctype html>
<html>
<head>
    @include('includes.head')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

    <header class="row">
        @include('includes.header')
    </header>
@include('includes.Sidebar')
    {{-- <div id="main" class="row">

            @yield('content')

    </div> --}}
    <div class="content-wrapper">
        @include('includes.contentheader')
        <div class="d-flex justify-content-center">
            <section class="content col-md-8 col-sm-12 ">
        <!-- Main content -->
        <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Add Payment</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              
              <div class="form-group">
                <label for="exampleInputBorder"> <code></code></label>
                <input type="text" id="txt_name" class="form-control form-control-border"  placeholder=".Search user">
              </div>
              <div id="replay"></div>
            </div>
            <!-- /.card-body -->
          </div>
        <!-- /.content -->
            </section>
        </div>
      </div>
      {{-- @include('includes.model')  --}}
      {{-- @include('includes.Addpayment_delete_model')  --}}
      {{-- @include('includes.Add_payment_return') --}}
      @include('includes.cashstatus')
      @include('includes.add_part_payment')
      @include('includes.delete_add_partpayment')
     
    <footer class="row">
        @include('includes.expand')
    </footer>

</div>
</body>
<script type="text/javascript">
    function fgh(){
     console.log('ghjhgj');
   }

    $(document).ready(function(){
         $( "#txt_name" ).keyup(function(e) {
          var data = { name:$.trim($(this).val())};
            $.ajax({
              type:'POST',
              url:"/findname",
              headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
              data:data,
              success:function(data){
               $('#replay').html(data)
                }
           });
         });
         $('#submitpart').click(function(e){
          var dataname = { amount:$('#partamount').val(), id:$('#submitpart').attr('data-amount'),round:$('#submitpart').attr('data-round'), partamountdate:$('#partamountdate').val(),divide:$('#submitpart').attr('data-divide'),catamount:$('#submitpart').attr('data-catamount'),addpayment_payments_unique_id:$('#submitpart').attr('data-addpayment_payments_unique_id')}
          let idcount = $('#submitpart').attr('data-round')+$('#submitpart').attr('data-amount');
           $.ajax({
          cache: true, 
          type:'POST',
          url:"/partAmount",
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          data:dataname,
          success:function(data){ 
           console.log(data);
          if(data.Error){ 
              $('#submitpartreplay').html('<label class="text-danger text-lg-center">'+data.Error+"</lable>");
              $('#partamount').val('')
              $('#partamountdate').val('')
         }else{ console.log('noterror'); 
          $(function () { $('#modal-sms').modal('toggle');}); 
          console.log(data.Sucess);
            if(data.Sucess.paydate!==null){
                $('<div class="row mt-3 hover" data-replayis='+data.Sucess.paymentdetail_unique_id+'><div class="col-2" data-total='+$('#submitpart').attr('data-round')+$('#submitpart').attr('data-amount')+'>'+data.Sucess.amount+'</div><div class="col-4">'+data.Sucess.paydate+'</div><div class="col-2">'+data.Sucess.status+'</div><div class="col-2"><div class="btn-group btn-sm" role="group" aria-label="Basic example"><button class="btn btn-danger btn-sm" type="button" data-toggle="modal" data-round="5" id='+data.Sucess.paymentdetail_unique_id+' data-id="" onclick="partpaymentdelete(\''+dataname.round+'\',\''+data.Sucess.paymentdetail_unique_id+'\',\''+dataname.id+'\',\''+data.Sucess.package_uniqueid+'\')" data-target=".bd-example-modal-sm">&times;</button> </div></div></div>').insertAfter("div[data-first="+$('#submitpart').attr('data-unque')+"]" );
               // $("#nom_equipe").append("<span data-total>"+data.nom_equipe+'</span>');
                 $('<span data-total='+i+'></span>').insertAfter("div[data-replayis="+data.Sucess.paymentdetail_unique_id+"]" );
                 var amount = parseInt($('#partamount').val());
                  var i = $('#submitpart').attr('data-round')+$('#submitpart').attr('data-amount');
                  var uniq=$('#submitpart').attr('data-unque');
                   pricecal(amount,i,uniq,$('#submitpart').attr('data-package'));
                 }
                }      
          }    
             
          }); 
         
           return false;
         });
      $('#submitpartdelete').click(function(e){
     var data = {unque:$('#submitpartdelete').attr('data-unque'),id:$('#submitpartdelete').attr('data-amount'),round:$('#submitpartdelete').attr('data-round')}
     $.ajax({
          cache: false, 
          type: "POST",
          async: true,
          url: "/DeleteAmount",
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          data: data,
          dataType: "json",
          success: function(data){
           console.log(data);
             
            $("div[data-replayis='"+$('#submitpartdelete').attr('data-amount')+"']" ).fadeOut("slow",function(){
                 $(this).remove();
              }).css('background-color', 'red');
             } });
             $(function () { $('#modal-smspartdelete').modal('toggle');});
            var amount = parseInt(0);
            var deleteamount = parseInt($("div[data-replayis='"+$('#submitpartdelete').attr('data-amount')+"']" ).children().first().html());
            var i = $('#submitpartdelete').attr('data-round')+$('#submitpartdelete').attr('data-unque');
            var uniq=$('#submitpartdelete').attr('data-to');
          console.log(amount);
          deletepricecal(amount,i,uniq,deleteamount);
         return false; 
         });
         //
         $('#submitcash').click(function(e){
     var data = {unque:$('#submitcash').attr('data-unque'),id:$('#submitcash').attr('data-amount'),round:$('#submitcash').attr('data-round')}
     $.ajax({
          cache: false, 
          type: "POST",
          async: true,
          url: "/CashAmount",
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          data: data,
          dataType: "json",
          success: function(data){
            $("div[data-replayis='"+$('#submitcash').attr('data-amount')+"']" ).remove();
            } });
            
            $(function () { $('#modal-cashdetail').modal('toggle');});
         return false; 
         });

         function pricecal(amount,i,uniq,datapackage){
              var total =0;
               $("div[data-total="+i+"]").each(function(index){
                  total +=  parseInt($(this).text());
               });
               
               var blance = parseInt(datapackage)-total;
               console.log(blance);
               $( "span[data-total="+uniq+"]" ).replaceWith( "<span data-total="+uniq+">Total="+total+"</span>" );//data-totalbal
               $( "span[data-totalbal="+uniq+"]" ).replaceWith( "<span data-totalbal="+uniq+">blance="+blance+"</span>" );
            //$('#partamount').val('');
         }
         function deletepricecal(amount,i,uniq,deleteamount){
                var total =0;
               $("div[data-total="+i+"]").each(function(index){
                  //console.log($(this).html());
                  total +=  parseInt($(this).text());
               });
               //console.log((total));
              var uo = parseInt($("div[data-packgeid="+uniq+"]").children().eq(1).text()); 
              var p = parseInt(total+amount)- parseInt(deleteamount)
            $( "span[data-total="+uniq+"]" ).text(p);
            $( "span[data-totalbal="+uniq+"]" ).replaceWith( "<span data-totalbal="+uniq+">blance="+(parseInt(uo-p))+"</span>" );
            //$('#partamount').val('');
         }

        

    
      /*
      $( "#submit" ).click(function(e) {
       e.preventDefault();
        let data = {amount:$('#amount').val(),id:$('#submit').attr('data-amount')}
        $.ajax({
        type:'POST',
        url:"/UpdateAmount",
        data:data,
        success:function(data){
        $('#amount').val('');
        $('#replaypayform').show();
          $('#replaypayform').html(data)
       }
    });
    });
    //return amount
        $( "#returnsubmit" ).click(function(e) {
        e.preventDefault();
        let data = {amount:$('#returnamount').val(),id:$('#returnsubmit').attr('data-amount')}
        $.ajax({
          type:'POST',
          url:"/ReturnAmount",
          data:data,
          success:function(data){
            $('#returnamount').val('');
            $('#replaypayreturnform').html(data)
          }
        });
          });
          $( "#returnsubmit" ).click(function(e) {
            e.preventDefault();
         let data = {amount:$('#returnamount').val(),id:$('#returnsubmit').attr('data-amount')}
            $.ajax({
              type:'POST',
              url:"/ReturnAmount",
              data:data,
              success:function(data){
                $('#returnamount').val('');
                $('#replaypayreturnform').html(data)
              }
            });
        });
      */
    });

   
   
   
   
    
</script>
</html>