<!doctype html>
<html>
<head>
    @include('includes.head')
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

    <header class="row">
        @include('includes.header')
    </header>
@include('includes.Sidebar')
    {{-- <div id="main" class="row">

            @yield('content')

    </div> --}}
    <div class="content-wrapper">
        @include('includes.contentheader')
        <div class="d-flex justify-content-center top-1">
         <section class="content col-md-8 col-sm-12 ">
            <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Member Registration</h3>
                </div> 
                <form method="post" action="{{url('registersub')}}" enctype="multipart/form-data">
                  {{ csrf_field() }}
                <div class="card-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Name</label>
                    <input type="name" name="name" class="form-control" id="exampleInputEmail1" placeholder="Name" required>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">email</label>
                  <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email" required>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Shop name</label>
                <input type="text" name="Shop_name" class="form-control" id="exampleInputEmail1" placeholder="Shop_name">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Phone number</label>
              <input type="tel" name="Phonenumber" class="form-control" id="exampleInputEmail1" placeholder="Phonenumber">
          </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Nic number</label>
                    <input type="text" class="form-control" name="Ic_number" id="exampleInputPassword1" placeholder="Ic number">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">password</label>
                  <input type="text" class="form-control" name="password"  id="password" placeholder="password">
              </div>
                <div class="form-group">
                    <label for="exampleInputFile">Profile picture</label>
                    <div class="input-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" name="image" id="exampleInputFile">
                        <label class="custom-file-label" for="exampleInputFile">Profile picture</label>
                    </div>
                    <div class="input-group-append">
                        <span class="input-group-text">Upload</span>
                    </div>
                    </div>
                </div>
                 
                </div>
                <!-- /.card-body -->
                  
                <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                </div>
               
            </form>
            @if($errors->any())
            <div class="alert top-2 alert-danger alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                @foreach ($errors->all() as $item)
                    {{$item}}<br/>
                @endforeach
               </div>
              @endif
              @if(session()->has('Successfull message'))
                <div class="alert top-2 alert-success alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ session()->get('Successfull message') }}
                </div>
              @endif
            <div class="card card-primary m-2">
                <div class="card-header ">
                  <h3 class="card-title">Memberes list</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="example1user" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>Name</th>
                      <th>email</th>
                      <th>Shop name</th>
                      <th>Phone number</th>
                      <th>Nic number</th>
                      {{-- <th>Roll</th> --}}
                      <th></th>
                      
                    </tr>
                    </thead>
                    <tbody>
                      
                      @foreach ($member as $item)
                         <tr id="{{$item->users_unique_id}}">
                           <td>{{$item->name}}</td>
                           <td>{{$item->email}}</td>
                           <td>{{$item->Shop_name}}</td>
                           <td>{{$item->Phonenumber}}</td>
                           <td>{{$item->Ic_number}}</td>
                           {{-- <td>{{$item->is_role==0?'User':$item->is_role==1?'Employee':'Admin'}}</td> --}}
                           <td> <?php 
                                     if($item->delete_permition==0)
                                     {
                                   ?>  
                             <a href="#" class="btn btn-default btn-sm" 
                               data-id="{{$item->id}}"
                               data-name="{{$item->name}}"
                               data-email="{{$item->email}}"
                               data-Shop_name="{{$item->Shop_name}}"
                               data-Phonenumber="{{$item->Phonenumber}}"
                               data-Ic_number="{{$item->Ic_number}}"
                               data-Profile_picture="{{$item->Profile_picture}}"
                               onclick="editmember(this)"><i class="fa fa-edit text-primary"></i></a>
                             <a href="#" class="btn btn-default btn-sm" 
                                data-id="{{$item->users_unique_id }}" onclick="deletemember(this)"><i class="fa fa-trash text-danger"></i></a>
                             {{-- <a href="{{ url('edit-member/'.$item->id) }}" class="btn btn-default btn-sm">
                              <i class="fa fa-trash text-danger"></i></a>
                              <a href="{{ url('edit-member/'.$item->id) }}" class="btn btn-default btn-sm m-1">
                                <i class="fa fa-edit text-primary"></i></a> --}}
                                <?php 
                                     }else{
                                ?>
                                
                                <a href="#" class="btn btn-default btn-sm m-1" data-id="{{$item->users_unique_id }}" onclick="restoremember(this)">
                                  <i class="fa fa-retweet text-primary" aria-hidden="true"></i></a>
                                <?php 
                                     }
                                ?>
                           
                          </td>
                         </tr> 
                      @endforeach
                   
                 
                    </tbody>
                  
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!--datatable-->
           </div>
            </div>   
        </section>
        </div>  
       
    </div> 
       
    <footer class="row">
        @include('includes.footer')
        @include('includes.datatable')
        @include('includes.edit_usermodelblade')
        @include('includes.delete_usermodelblade')
        @include('includes.restore_memeberblade')
    </footer>
   <script>
     function editmember(e){
      $('#upademember').fadeOut();
      $('#modal-user').modal('show');
      $('#id').val(e.getAttribute("data-id"));
      $('#name').val(e.getAttribute("data-name"));
      $('#email').val(e.getAttribute("data-email"));
      $('#Shop_name').val(e.getAttribute("data-Shop_name"));
      $('#Phonenumber').val(e.getAttribute("data-Phonenumber"));
      $('#Ic_number').val(e.getAttribute("data-Ic_number"));
      $('#imgname').val(e.getAttribute("data-Profile_picture"));
      console.log(e.getAttribute("data-name"));
     }
     function deletemember(e){
      $('#deletemember').fadeOut();
      $('#modal-delete_usermodelblade').modal('show');
      $('#submituserdelete').attr('data-id', e.getAttribute("data-id"));
     }
     
     function restoremember(e){
      $('#modal-restore_usermodelblade').modal('show');
      $('#submituserrestore').attr('data-id', e.getAttribute("data-id"));
     }
     //submituserrestore
     $("#submituserrestore").click('submit', function(e){
       let dataname = { id:$('#submituserrestore').attr('data-id')} ;
       console.log(dataname);
      e.preventDefault();
      $.ajax({
            type: 'POST',
            url: "/restoremember",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: dataname,
            dataType: 'json',
           success: function(data){
            if(data.Error){ 
              
                $("#restoremember").html('<label class="text-danger text-lg-center">'+data.Error+"</lable>").fadeIn();
            }else{
              $("#restoremember").html('<label class="text-success text-lg-center">'+data.Sucess+"</lable>").fadeIn();
                location.reload();
               }
          }
        })
       });

     //submituserdelete
     $('#submituserdelete').click(function(e){
          let dataname = { id:$('#submituserdelete').attr('data-id')} ;
          e.preventDefault();
      $.ajax({
        cache: true, 
          type:'POST',
          url:"/deletemember",
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          data:dataname,
          success: function(data){
            if(data.Error){ 
               $("#deletemember").html('<label class="text-danger text-lg-center">'+data.Error+"</lable>").fadeIn();
            }else{
                $("#deletemember").html('<label class="text-success text-lg-center">'+data.Sucesses+"</lable>").fadeIn();
                  location.reload();
                  $(function () { $('#modal-delete_usermodelblade').modal('toggle');}); 
                $("#example1user tbody ").find('#'+$('#submituserdelete').attr('data-id')).remove();
               }
          }
        })
         

     });
    $(document).ready(function(e){
      $("#fupForm").on('submit', function(e){
      e.preventDefault();
      $.ajax({
            type: 'POST',
            url: "/updatemember",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: new FormData(this),
            dataType: 'json',
            contentType: false,
            cache: false,
            processData:false,

          success: function(data){
            if(data.Error){ 
              
                $("#upademember").html('<label class="text-danger text-lg-center">'+data.Error+"</lable>").fadeIn();
            }else{
              $("#upademember").html('<label class="text-success text-lg-center">'+data.Sucess+"</lable>").fadeIn();
                location.reload();
               }
          }
        })
       });
   });
  
   </script>


</div>
</body>
</html>