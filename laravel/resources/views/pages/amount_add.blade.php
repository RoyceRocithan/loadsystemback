<!doctype html>
<html>
<head>
    @include('includes.head')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

    <header class="row">
        @include('includes.header')
    </header>
@include('includes.Sidebar')
    {{-- <div id="main" class="row">

            @yield('content')

    </div> --}}
    <div class="content-wrapper">
        @include('includes.contentheader')
        <div class="d-flex justify-content-center">
            <section class="content col-md-8 col-sm-12 ">
        <!-- Main content -->
        <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Add Amount</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              
              <div class="form-group">
                <label for="exampleInputBorder"> <code></code></label>
                <input type="text" id="txt_name" class="form-control form-control-border"  placeholder=".find the user">
              </div>
              <div id="replay"></div>
              <div id="replay_two"></div>
            </div>
            <!-- /.card-body -->
          </div>
        <!-- /.content -->
            </section>
        </div>
      </div>
    

     
    <footer class="row">
        @include('includes.expand')
    </footer>

</div>
</body>
<script>
    $(document).ready(function(){
         $( "#txt_name" ).keyup(function(e) {
          var data = { name:$.trim($(this).val())};
             $.ajax({
              type:'POST',
              url:"/finduser",
              headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
              data:data,
              success:function(data){
               $('#replay').html(data)
                }
           });
         });
        });
        function find_amount(name,id){
         
        let data = {name:name,id:id};
        $.ajax({
              type:'POST',
              url:"/importAmount",
              headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
              data:data,
              success:function(data){
               $('#replay_two').html(data)
                }
           });
       
        }
        function acform(){
          let data = {id:$('#id').val(),amount:$('#amount').val()}
          $.ajax({
              type:'POST',
              url:"/addacAmount",
              headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
              data:data,
              success:function(data){
               $('#replay_two').html(data)
                }
           });

        }

</script>