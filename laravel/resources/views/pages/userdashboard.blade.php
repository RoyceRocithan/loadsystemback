<!doctype html>
<html>
<head>
    @include('includes.head')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
        <?php 
         use Illuminate\Support\Facades\Auth;
         use App\Models\addpayment;
         use App\Models\User;
        use App\Models\Category;
        use App\Models\Payment;

        ?>
    <header class="row">
        @include('includes.userheader')
    </header>
@include('includes.userSlider')
    {{-- <div id="main" class="row">

            @yield('content')

    </div> --}}
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1><?=Route::getFacadeRoot()->current()->uri();?></h1>
              
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/logout">logout</a></li>
                {{-- <li class="breadcrumb-item active">Project Detail</li> --}}
              </ol>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>
  
      <!-- Main content -->
      <section class="content">

        <!-- Default box -->
        {{-- <div class="card">
        <div class="card-header">
          <h3 class="card-title">Credit Report Detail</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
          <?php 
           // print_r(Auth::user()->users_unique_id)
          ?>
          {{-- <table class="table" id="example1">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Package</th>
                <th scope="col">Amount</th>
               </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row">1</th>
                <td>55</td>
                <td>66</td>
               
              </tr>
              <tr>
                <th scope="row">2</th>
                <td>Jacob</td>
                <td>Thornton</td>
                
              </tr>
              <tr>
                <th scope="row">2</th>
                <td>Jacob</td>
                <td>Thornton</td>
                
              </tr>
            </tbody>
          </table> --}}
       {{-- </div>
        </div> --}}
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">payment Report Detail</h3>
  
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
              </button>
            </div>
          </div>
          <div class="card-body">
            <?php 
              $payment = addpayment::select('*')
                       ->where('addpayments.payment_status','active')
                       ->where('users.users_unique_id',Auth::user()->users_unique_id)
                      ->leftJoin('payments', 'payments.payments_unique_id', '=', 'addpayments.addpayment_payments_unique_id')
                      ->leftJoin('categories','categories.categories_unique_id','=','payments.payments_category_id')
                       ->leftJoin('users','users.users_unique_id','=','addpayments.addpayment_user_unique_id')
                       ->get()
                       ->toArray();
                      //  print('<pre>');
                      //  print_r($payment);
                      //  print('</pre>');
            ?>
             <table id="example1" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>UserName</th>
                <th>packageamout</th>
                <th>Round</th>
                <th>Credit amount</th>
              </tr>
              </thead>
              <tbody>
                @foreach ($payment as $item)
                  <tr>
                      <td>{{$item['name']}}</td>
                      <td>{{$item['amount']}}</td>
                      <td>
                        <?php  
                         $yfd =[];
                          $u = ($item['part_payment_detatil']);
                         $ghg= json_decode($u);
                         foreach ($ghg as $key => $value) {
                            foreach ($value as $key => $value1) {
                                  $iof = json_decode(json_encode($value1),true)  ;
                                  foreach ($iof as $key => $value2) {
                                      $yg= (int)$value2['packageDiscription']['packageamount'];
                                     if($value2['packageDiscription']['packagestatus']==='processing'){  
                                          //  print_r($value2['packageDiscription']['packagestatus']);  
                                         array_push($yfd,$value2['packageDiscription']['packagestatus']);                                              

                                     }
                                     
                                     
                                  }
                                  
                            }
                         }
                      echo count($yfd);
                         ?>

                      </td>
                      <td><?php  
                        $t =0;
                        $y =0;
                        $u = ($item['part_payment_detatil']);
                         $ghg= json_decode($u);
                         foreach ($ghg as $key => $value) {
                            foreach ($value as $key => $value1) {
                                  $iof = json_decode(json_encode($value1),true)  ;
                                  foreach ($iof as $key => $value2) {
                                     $yg= $value2['packageDiscription']['packagestatus']==='Complete'?NULL:(int)$value2['packageDiscription']['packageamount'];
                                       $y+=$yg;
                                     if($value2['packageDiscription']['packagestatus']==='processing'){  
                                           foreach ($value2['paymentdetail'] as $key => $value5) {
                                                     $p = (int)$value5['amount'];
                                                    $t+=$p;
                                                }
                                      
                                     }
                                     
                                  }
                                  
                            }
                         }
                         print_r($y-$t);
                         ?>
                      </td> 
                  </tr>  
                @endforeach
              </tbody>
            
            </table> 
            <?php 
              print_r(Auth::user()->users_unique_id)
            ?>
          </div>
          </div>

         <!-- Default box -->
        
             
  
        <!-- Default box -->
        
        <!-- /.card -->
         
      </section>
      <!-- /.content -->
    </div>
     

    <footer class="row">
        @include('includes.footer')
        @include('includes.datatable')
    </footer>

</div>
</body>
</html>