<!doctype html>
<html>
<head>
    @include('includes.head')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

    <header class="row">
        @include('includes.header')
        <?php 
        use Illuminate\Support\Facades\Auth;
        use App\Models\addpayment;
        use App\Models\User;
        use App\Models\Category;
        use App\Models\Payment;

       ?>
    </header>
@include('includes.Sidebar')
    {{-- <div id="main" class="row">

            @yield('content')

    </div> --}}
    <div class="content-wrapper">
        @include('includes.contentheader')
    
        <!-- Main content -->
        <section class="content">
          <div class="card col-10 ml-5 card-primary card-outline">
            <div class="card-body box-profile">
              <div class="text-center">
                <img class="img-circle img-bordered-sm" src="<?=Auth::user()->Profile_picture==''?'AdminLTE-master/dist/img/user2-160x160.jpg'
                          :Auth::user()->Profile_picture?>" width="160" height="160" alt="user image">
                          <span class="username">

              </div>
          
              <h3 class="profile-username text-center"><?=Auth::user()->name?></h3>
          
              {{-- <p class="text-muted text-center"><?=//Auth::user()->is_role==2?'Admin':Auth::user()->is_role==1?'Employ':'user'?></p> --}}
          
              <ul class="list-group list-group-unbordered mb-3">
                <li class="list-group-item">
                  <b>E-mail</b><a class=" float-right"><?=Auth::user()->email?></a>
                </li>
                <li class="list-group-item">
                  <b>S-hopname</b> <a class=" float-right"><?=Auth::user()->Shop_name?></a>
                </li>
                <li class="list-group-item">
                  <b>P-number</b> <a class=" float-right"><?=Auth::user()->Phonenumber?></a>
                </li>
                <li class="list-group-item">
                  <b>N-icnumber</b> <a class=" float-right"><?=Auth::user()->Ic_number?></a>
                </li>
              </ul>
          
             
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
      </div>
     

    <footer class="row">
        @include('includes.footer')
    </footer>

</div>
</body>
</html>




