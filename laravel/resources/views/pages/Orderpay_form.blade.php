<!doctype html>
<html>
<head>
    @include('includes.head')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

    <header class="row">
        @include('includes.header')
    </header>
@include('includes.Sidebar')
    {{-- <div id="main" class="row">

            @yield('content')

    </div> --}}
    <div class="content-wrapper">
        @include('includes.contentheader')
        <div class="d-flex justify-content-center">
            <section class="content col-md-6 col-sm-12 ">
                <div class="card card-info">
                    <div class="card-header">
                      <h3 class="card-title">Horizontal Form</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form method="post" action="{{url('oderpay_sub')}}" enctype="multipart/form-data">
                      {{ csrf_field() }}
                      <div class="card-body">
                        @foreach ($data as $item)
                        
                        <div class="form-group row">
                          <label for="inputEmail3" class="col-sm-2 col-form-label">Amount</label>
                          <div class="col-sm-10">
                              <div class="row">
                                   <div class="col-8">
                                     <input type="text" disabled class="form-control" name="oder[]" id="inputEmail3" value=" {{$item->amount}}">
                                     <input type="hidden"  class="form-control" name="oderid[]" id="inputEmail3" value=" {{$item->categories_unique_id}}">
                                    </div>
                                   <div class="col-4">
                                    <select class="form-control" name="oderpay[]">
                                     <?php 
                                        for ($i=1; $i <=count($data) ; $i++) { 
                                      ?>
                                       <option value="{{$i}}"><?=$i?></option>
                                     <?php
                                        }  
                                      ?>
                                      </select>
                                    
                                    </div>
                              </div>
                           
                          </div>
                        </div>
                        @endforeach                   
                      </div>
                      <!-- /.card-body -->
                      <div class="card-footer">
                      <button type="submit" class="btn btn-default float-right">Cancel</button>
                      <button type="submit" class="btn btn-info">Sign in</button></div>
                      <!-- /.card-footer -->
                    </form>
                    @if($errors->any())
                    <div class="alert top-2 alert-danger alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      {{ implode('', $errors->all()) }}
                       </div>
                      @endif
                     
                      @if(session()->has('Successfull message'))
                      <div class="alert top-2 alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                          {{ session()->get('Successfull message') }}
                      </div>
                    @endif
                  </div>
                  <form method="post" action="{{url('oderpay_delete')}}" enctype="multipart/form-data">
                    {{ csrf_field() }} 
                  <button type="submit" class="btn btn-danger btn-lg btn-block m-1">Delete All recodes...</button>
                  </form>
                   <table id="example1" class="table table-bordered table-striped mt-2">
                  <thead>
                  <tr>
                    <th>Amount</th>
                    <th>oderby</th>
                    
                  </tr>
                  </thead>
                  <tbody>
                    @foreach ($query as $item)
                    <tr>
                    <td>{{$item->amount}}</td>
                    <td>{{$item->oder}}</td>
                     </tr> 
                    @endforeach
                  
                 
                  
                  </tbody>
                
                </table> 
        <!-- Main content -->
               {{-- @foreach ($data as $item)
                   {{$item->amount}}
               @endforeach --}}
               
        <!-- /.content -->
            </section>
        </div>
      </div>
    
    <footer class="row">
        @include('includes.expand')
        @include('includes.datatable')
    </footer>

</div>
</body>

</html>