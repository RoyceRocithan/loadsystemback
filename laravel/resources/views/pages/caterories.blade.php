<!doctype html>
<html>
<head>
    @include('includes.head')
    @php
    use App\Models\User;
    
    @endphp

</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

    <header class="row">
        @include('includes.header')
    </header>
@include('includes.Sidebar')
    {{-- <div id="main" class="row">

            @yield('content')

    </div> --}}
    <div class="content-wrapper">
        @include('includes.contentheader')
        <div class="d-flex justify-content-center">
         <section class="content col-md-8 col-sm-12 ">
            <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">pay-add-Amount</h3>
                  
                </div> 
               
               
                <div class="card card-default">
                    <div class="card-header">
                      <h3 class="card-title"></h3>
          
                      <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                          <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                          <i class="fas fa-times"></i>
                        </button>
                      </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                      <div class="row">
                        <div class="col-12">
                          <form method="post" action="{{url('cateories_sub')}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                          <div class="card-body">
                          <div class="form-group">
                              <label for="exampleInputEmail1">Cat-Amount</label>
                              <input type="number" name="amount" class="form-control" id="exampleInputEmail1" placeholder="Cat-Amount">
                          </div>
                          </div>
                          <!-- /.card-body -->
                            
                          <div class="card-footer">
                          <button type="submit" class="btn btn-primary">Submit</button>
                          
                          </div>
                      </form> 
                                      
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                      
                      @if($errors->any())
            <div class="alert top-2 alert-danger alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              {{ implode('', $errors->all()) }}
               </div>
              @endif
             
              @if(session()->has('Successfull message'))
              <div class="alert top-2 alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  {{ session()->get('Successfull message') }}
              </div>
            @endif
                    </div>
                  </div>
                    <!-- /.form-group -->
                  
             
            
            <div class="card card-primary m-2">
                <div class="card-header ">
                  <h3 class="card-title">pay-add-Amount report</h3>
                </div>
                {{-- {{$payment}} --}}
                <!-- /.card-header -->
                <div class="card-body">
                   <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>Amount</th>
                      <th>Date</th>
                      <th>CreateBy</th>
                      <th></th>
                    </tr>
                    </thead>
                    <tbody>
                      @foreach ($data as $item)
                      <tr>
                      <td>{{$item->amount}}</td>
                      <td>{{date('d-m-Y', strtotime($item->created_at))}}</td>
                      <td>{{$item->name}}</td>
                      <td><button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash text-danger"></i></button>
                        <button type="button" class="btn btn-default btn-sm m-1"><i class="fa fa-edit text-primary"></i></button>
                      </td>
                     
                    </tr> 
                      @endforeach
                    
                   
                    
                    </tbody>
                  
                  </table> 
                </div>
                <!-- /.card-body -->
              </div>
              <!--datatable-->
           </div>
            </div>   </div>
        </section>
        </div>  
       
    </div> 

    <footer class="row">
        @include('includes.footer')
        @include('includes.datatable')
    </footer>

</div>
</body>
 <script>
 
 </script>
</html>