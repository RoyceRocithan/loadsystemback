<!doctype html>
<html>
<head>
    @include('includes.head')
    @php
    use App\Models\User;
    
    @endphp

</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

    <header class="row">
        @include('includes.header')
    </header>
@include('includes.Sidebar')
    {{-- <div id="main" class="row">

            @yield('content')

    </div> --}}
    <div class="content-wrapper">
        @include('includes.contentheader')
        <div class="d-flex justify-content-center">
         <section class="content col-md-8 col-sm-12 ">
            <div class="card card-primary m-2">
                <div class="card-header ">
                  <h3 class="card-title">Round</h3>
                </div>
                {{-- {{$payment}} --}}
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                       <th>Amount</th>
                       <th>CreateBy</th>
                       <th>Status</th>
                      <th></th>
                    </tr>
                    </thead>
                    <tbody>
                       @foreach ($payment as $item)
                       <tr>
                      <td>{{$item->amount}}</td>
                       <td>{{$item->name}}</td>
                       <td>{{$item->payment_status}}</td>
                       <td> <button type="button" class="btn btn-default btn-sm m-1"><i id="submitround" class="fa fa-edit text-primary" data-id="{{$item->payments_unique_id}}"  onclick="findall(this)">Add Round</i></button></td>
                       </tr>
                       @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!--datatable-->
           </div>
            </div>   </div>
        </section>
        </div>  
       
    </div> 
    @include('includes.round_model');
    @include('includes.confirmround_model');
    <footer class="row">
        @include('includes.footer')
        @include('includes.datatable')
    </footer>

</div>
</body>
 <script>
  function findall(e){
    $.ajaxSetup({headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        'Cache-Control': 'no-cache, no-store, must-revalidate', 
                        'Pragma': 'no-cache', 
                        'Expires': '0'
                          } });
    var data = {id:e.getAttribute("data-id")}
    console.log(data);
    $.ajax({
    cache: false, 
    type: "POST",
    async: true,
    url: "/roundadd",
    data: data,
    dataType: "json",
    success: function(data){
      console.log(data);
       $('#replayround').html(data.data)
       } 
  
       });
            $('#modal-smsround').modal('show');
           }
    function activateround(round,unique,packageid,package_payid,packageamount,packagestatus,total_me,cat_amount,Divide) {
      var o = $("select#"+packageid+" option").filter(":selected").val();
      var username = $("select#"+packageid+" option").filter(":selected").text();
      var t = $("#"+packageid).val(); 
      const userkeylist =[];
      var u= $( "div[data-userdetail]" ).each(function( index ) {userkeylist.push($(this).attr("data-userdetail"));});
       var data = {round:round,packageid:packageid,unique:unique,userid:o,amount:t,package_payid:package_payid,packageamount:packageamount,packagestatus:packagestatus,userselectkey:userkeylist,total_me:total_me,cat_amount:cat_amount,Divide:Divide,username:username}
      $.ajax({
     cache: false, 
     type: "POST",
     async: true,
     url: "/withdorw",
     data: data,
     dataType: "json",
    success: function(data){
      if(data.Error){
        console.log(data.Error);
      $('#replayactiveround').html(data.Error)
       $('#modal-smsactiveround').modal('show');
      }else{
        console.log(data.sucess[0][0]);
       $('<div class="row mt-3 hover" data-replayis='+data.sucess[0][0].paymentdetail_unique_id+'><div class="col-2" data-total="">'+data.sucess[0][0].amount+'</div><div class="col-4">'+data.sucess[0][0].get_date+'</div><div class="col-2">'+data.sucess[0][0].username+'</div><div class="col-2"><div class="btn-group btn-sm" role="group" aria-label="Basic example"></div></div></div>').insertAfter("div[data-first="+packageid+"]" );
      }
      
    } 
  
       });
      
   //$('#modal-smsactiveround').modal('show');
   }
    
 </script>
</html>