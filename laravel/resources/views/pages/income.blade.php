<!doctype html>
<html>
<head>
    @include('includes.head')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

    <header class="row">
        @include('includes.header')
    </header>
@include('includes.Sidebar')
<?php 
 use App\Models\User;
 use App\Models\Payment;
 use App\Models\addpayment;
 use App\Models\Category;

 ?> 
    {{-- <div id="main" class="row">

            @yield('content')

    </div> --}}
    <div class="content-wrapper">
        @include('includes.contentheader')
    
        <!-- Main content -->
        <section class="content">
            <?php 
            $payment = addpayment::select('*')
                       ->where('addpayments.payment_status','active')
                      ->leftJoin('payments', 'payments.payments_unique_id', '=', 'addpayments.addpayment_payments_unique_id')
                      ->leftJoin('categories','categories.categories_unique_id','=','payments.payments_category_id')
                       ->leftJoin('users','users.users_unique_id','=','addpayments.addpayment_user_unique_id')
                       ->get()
                       ->toArray();
                    //    print('<pre>');
                    //    print_r($payment);
                    //    print('</pre>');
           
            
           ?>
           <div class="card card-primary m-2">
            <div class="card-header ">
              <h3 class="card-title">User Detail</h3>
            </div>
            {{-- {{$payment}} --}}
            <!-- /.card-header -->
            <div class="card-body">
               <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>UserName</th>
                  <th>packageamout</th>
                  <th>Round</th>
                  <th>Credit amount</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($payment as $item)
                    <tr>
                        <td>{{$item['name']}}</td>
                        <td>{{$item['amount']}}</td>
                        <td>
                          <?php  
                           $yfd =[];
                            $u = ($item['part_payment_detatil']);
                           $ghg= json_decode($u);
                           foreach ($ghg as $key => $value) {
                              foreach ($value as $key => $value1) {
                                    $iof = json_decode(json_encode($value1),true)  ;
                                    foreach ($iof as $key => $value2) {
                                        $yg= (int)$value2['packageDiscription']['packageamount'];
                                       if($value2['packageDiscription']['packagestatus']==='processing'){  
                                            //  print_r($value2['packageDiscription']['packagestatus']);  
                                           array_push($yfd,$value2['packageDiscription']['packagestatus']);                                              

                                       }
                                       
                                       
                                    }
                                    
                              }
                           }
                        echo count($yfd);
                           ?>

                        </td>
                        <td><?php  
                          $t =0;
                          $y =0;
                          $u = ($item['part_payment_detatil']);
                           $ghg= json_decode($u);
                           foreach ($ghg as $key => $value) {
                              foreach ($value as $key => $value1) {
                                    $iof = json_decode(json_encode($value1),true)  ;
                                    foreach ($iof as $key => $value2) {
                                       $yg= $value2['packageDiscription']['packagestatus']==='Complete'?NULL:(int)$value2['packageDiscription']['packageamount'];
                                         $y+=$yg;
                                       if($value2['packageDiscription']['packagestatus']==='processing'){  
                                             foreach ($value2['paymentdetail'] as $key => $value5) {
                                                       $p = (int)$value5['amount'];
                                                      $t+=$p;
                                                  }
                                        
                                       }
                                       
                                    }
                                    
                              }
                           }
                           print_r($y-$t);
                           ?>
                        </td> 
                    </tr>  
                  @endforeach
                </tbody>
              
              </table> 
            </div>
            <!-- /.card-body -->
          </div>
        </section>
        <!-- /.content -->
      </div>
     

    <footer class="row">
        @include('includes.footer')
        @include('includes.datatable')
    </footer>

</div>
</body>
</html>