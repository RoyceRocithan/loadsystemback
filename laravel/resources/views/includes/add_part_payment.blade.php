<div class="modal fade" id="modal-sms">
  <div class="modal-dialog modal-dialog-centered modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Part Amount</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <form id="partpayment">
        <div class="form-group">
          <label for="exampleInputBorder"> <code></code></label>
          <input type="date" id="partamountdate" name="partamountdate" required class="form-control form-control-border"  placeholder="Partamount"><br>
          <input type="number" id="partamount" name="partamount" class="form-control form-control-border"  placeholder="Amount">
          <button id="submitpart" class=" float-right btn-sm btn btn-primary mt-2" data-round="" data-amount="" data-unque="" data-package="" data-divide="" data-catamount="" data-addpayment_payments_unique_id="">Add</button>
        </div>
        <div id="submitpartreplay"></div>
       </form>
       <!--<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>-->
      </div>
      </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<script>
     function findpartpayment(e){
      $('#submitpartreplay').html('');
      $('#partamount').val('');
      $('#partamountdate').val('');
     $('#submitpart').attr('data-amount', e.getAttribute("data-id"));
     $('#submitpart').attr('data-round', e.getAttribute("data-round"));
     $('#submitpart').attr('data-unque', e.getAttribute("data-unque"));
     $('#submitpart').attr('data-package', e.getAttribute("data-package"));
     $('#submitpart').attr('data-package', e.getAttribute("data-package"));
     $('#submitpart').attr('data-package', e.getAttribute("data-package"));
     $('#submitpart').attr('data-divide', e.getAttribute("data-divide"));
     $('#submitpart').attr('data-catamount', e.getAttribute("data-catamount"));
     $('#submitpart').attr('data-addpayment_payments_unique_id', e.getAttribute("data-addpayment_payments_unique_id"));
     $('#modal-sms').modal('show');
     
    }
</script>  
   