<div class="modal fade" id="modal-smspartdelete">
    <div class="modal-dialog modal-dialog-centered modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Delete Amount</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <p >Delete this Record ?.</p>
         <!--<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>-->
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="submitpartdelete" data-amount="" data-round="" data-unque="" data-to="" >Delete</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div> <div id="submitpartdelete"></div>
        </div>
       
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <script>
       function partpaymentdelete(e,o,s,t){
        
        var round = e; //61ea952636e19
        var dataid = o;
        var dataunque=s;
        var total = t;
      $('#replaypayform').hide();
      $('#submitpartdelete').attr('data-amount',dataid);
      $('#submitpartdelete').attr('data-round', e);
      $('#submitpartdelete').attr('data-unque', dataunque);
      $('#submitpartdelete').attr('data-to', t);
     $('#modal-smspartdelete').modal('show');
     
    }
   
  </script>