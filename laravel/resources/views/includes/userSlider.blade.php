<?php
  use Illuminate\Support\Facades\Auth;
?>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ URL::to('userdashboard') }}" class="brand-link">
      <img src="AdminLTE-master/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Dash Board</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          {{-- <img src="AdminLTE-master/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image"> --}}
          <img src="<?=Auth::user()->Profile_picture==''?'AdminLTE-master/dist/img/user2-160x160.jpg':Auth::user()->Profile_picture?>" width="160" height="160" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="/userprofile" class="d-block"><?=Auth::user()->name?></a>
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item menu-open">
            <a href="{{ URL::to('userdashboard') }}" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                {{-- <i class="right fas fa-angle-left"></i> --}}
              </p>
            </a>
            {{-- <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="./index.html" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Dashboard v1</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="./index2.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Dashboard v2</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="./index3.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Dashboard v3</p>
                </a>
              </li>
            </ul> --}}
          </li>
          {{-- <li class="nav-item">
            <a href="{{ URL::to('member') }}" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p> Member </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ URL::to('caterories') }}" class="nav-link">
              <i class="nav-icon fas fa-address-book"></i>
              <p> Caterories </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/Add_cat_money_form" class="nav-link">
              <i class="nav-icon fa-id-card"></i>
              <p> Add_cat_money </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/Add_payment" class="nav-link">
              <i class="nav-icon fa-id-card"></i>
              <p> Add_payment </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/round" class="nav-link">
              <i class="nav-icon fa-hand-paper-o"></i></i>
              <p> Active round </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/expensive" class="nav-link">
              <i class="nav-icon fa-id-card"></i>
              <p> expensive </p>
            </a>
          </li> --}}
         

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>