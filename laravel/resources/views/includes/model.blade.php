<div class="modal fade" id="modal-smss">
  <div class="modal-dialog modal-dialog-centered modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Amount</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <form id="partpayment">
        <div class="form-group">
          <label for="exampleInputBorder"> <code></code></label>
          <input type="number" id="amount" name="amount" class="form-control form-control-border"  placeholder="Amount">
           <button id="submit" class=" float-right btn-sm btn btn-primary mt-2" data-amount="">Add</button>
        </div>
        <div id="replaypayform"></div>
       </form>
      
      </div>
      </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<script>
     function find(e){
      $('#replaypayform').hide();
     $('#submit').attr('data-amount', e.getAttribute("data-id"));
     $('#modal-smss').modal('show');
     
    }
</script>  
   