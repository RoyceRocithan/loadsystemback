<div class="modal fade" id="modal-restore_usermodelblade">
    <div class="modal-dialog modal-dialog-centered modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Restore This user</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <p >Restore this User ?.</p>
         <!--<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>-->
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="submituserrestore" data-id="" >Restore</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div> <div id="restoremember"></div>
        </div>
       
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  