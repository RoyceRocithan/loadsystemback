<div class="modal fade" id="modal-delete_usermodelblade">
    <div class="modal-dialog modal-dialog-centered modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Delete Amount</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <p >Delete this Record ?.</p>
         <!--<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>-->
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="submituserdelete" data-id="" >Delete</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div> <div id="deletemember"></div>
        </div>
       
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  