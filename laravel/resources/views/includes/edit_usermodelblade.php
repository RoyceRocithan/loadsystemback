<div class="modal fade" id="modal-user">
    <div class="modal-dialog modal-dialog-centered modal-xl">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Edit Member</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title"> Edit Member </h3>
                </div> 
                <form  action="#" enctype="multipart/form-data" name="myForm" id="fupForm">
                <div class="card-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Name</label>
                    <input type="text" name="name" id="name" class="form-control"  placeholder="Name" required>
                    <input type="hidden" name="id" id="id" class="form-control"  placeholder="Name" required>
                    <input type="hidden" name="imgname" id="imgname" class="form-control"  placeholder="Name" required>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">email</label>
                  <input type="email" name="email" id="email" class="form-control"  placeholder="Enter email" required>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Shop name</label>
                <input type="text" name="Shop_name" id="Shop_name" class="form-control"  placeholder="Shop_name">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Phone number</label>
              <input type="tel" name="Phonenumber" id="Phonenumber" class="form-control"  placeholder="Phonenumber">
          </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Nic number</label>
                    <input type="text" class="form-control" id="Ic_number" name="Ic_number"  placeholder="Ic number">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">password</label>
                  <input type="text" class="form-control" id="password" name="password"  id="password" placeholder="password">
              </div>
                <div class="form-group">
                    <label for="exampleInputFile">Profile picture</label>
                    <div class="input-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" name="image" id="image">
                        <label class="custom-file-label" for="exampleInputFile">Profile picture</label>
                    </div>
                    <div class="input-group-append">
                        <span class="input-group-text">Upload</span>
                    </div>
                    </div>
                </div>
                 <div id="upademember"></div>
                </div>
                <!-- /.card-body -->
                  
                <div class="card-footer">
                <input type="submit" name="submit" class="btn btn-success submitBtn" value="Update"/>
                </div>
               
            </form>
        </div>
        
        </div>
        </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>